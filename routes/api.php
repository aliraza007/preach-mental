<?php

use App\Http\Controllers\Api\v1\Ayat\AyatController;
use App\Http\Controllers\Api\v1\Hadith\HadithController;
use App\Http\Controllers\Api\v1\Leader\LeaderController;
use App\Http\Controllers\Api\v1\Leader\LeaderDetailController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\User\AuthController;
use App\Http\Controllers\Api\v1\User\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use Carbon\Carbon;

Route::group(['prefix' => 'v1'], function() {
    Route::post("login", [AuthController::class, 'login']);
    Route::post("register", [AuthController::class, 'signUp']);
    Route::post('forgot-password',[AuthController::class, 'forgotPassword']);
    Route::post('new-password',[AuthController::class, 'newPassword']);
    Route::post('social/login',[AuthController::class, 'socialLogin']);
});


Route::middleware('auth:api')->prefix('v1')->group(function() {
	Route::post("logout", [UserController::class, 'logout']);
	Route::get("me", [UserController::class, 'getUserDetail']);
});
Route::middleware('auth:api')->prefix('v1')->group(function() {
	Route::get("leader", [LeaderController::class, 'getLeader']);
	Route::get("leader-details", [LeaderDetailController::class, 'getLeaderDetails']);
	Route::get("hadith", [HadithController::class, 'getHadith']);
	Route::get("ayat", [AyatController::class, 'getAyat']);
});

