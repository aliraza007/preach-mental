<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Ayat;
use App\Models\AyatDetail;

class AyatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ayat = new Ayat();
        $ayat->title = "Anger";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>42,
                'ayat_number' =>"37",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Ash-Shura ',
                'title_english' =>'When they are wroth, forgive',
                'title_urdu' =>'اور جب غصہ آتا ہے تو معاف کردیتے ہیں',
                'title_arabic' =>'وَاِذَا مَا غَضِبُوۡا هُمۡ يَغۡفِرُوۡنَ',
                'description' => "Allah has advised us to repress our anger in all situations, no matter how testing they are. He has mentioned this specifically in the Quran, He says to forgive when you are in anger:",
                'created_at' => now(),
            )
        );

        $ayat = new Ayat();
        $ayat->title = "Anxious";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>21,
                'ayat_number' =>"87",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Al-Anbya',
                'title_english' =>'There is no Allah save Thee. Be Thou Glorified! Lo! I have been a wrong-doer',
                'title_urdu' =>'تیرے سوا کوئی معبود نہیں۔ تو پاک ہے (اور) بےشک میں قصوروار ہوں',
                'title_arabic' =>'لَّآ إِلَـٰهَ إِلَّآ أَنتَ سُبْحَـٰنَكَ إِنِّى كُنتُ مِنَ ٱلظَّـٰلِمِينََ',
                'description' => "Here are dua that you may want to recite in facing anxiety:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>20,
                'ayat_number' =>"25-26",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Taha ',
                'title_english' =>'My Lord! Relieve my mind And ease my task for me',
                'title_urdu' =>'میرے پروردگار (اس کام کے لئے) میرا سینہ کھول دے اور میرا کام آسان کردے',
                'title_arabic' =>'رَبِّ اشۡرَحۡ لِىۡ صَدۡرِىۙ وَيَسِّرۡ لِىۡۤ اَمۡرِىۙ ',
                'description' => null,
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>60,
                'ayat_number' =>"4",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Al-Mumtahanah ',
                'title_english' =>' Our Lord! In Thee we put our trust, and unto Thee we turn repentant, and unto Thee is the journeying',
                'title_urdu' =>'اے ہمارے پروردگار تجھ ہی پر ہمارا بھروسہ ہے اور تیری ہی طرف ہم رجوع کرتے ہیں اور تیرے ہی حضور میں (ہمیں) لوٹ کر آنا ہے',
                'title_arabic' =>'رَّبَّنَا عَلَيۡكَ تَوَكَّلۡنَا وَاِلَيۡكَ اَنَـبۡنَا وَاِلَيۡكَ الۡمَصِيۡ',
                'description' => "Dua for attaining confidence in Allah SAW:",
                'created_at' => now(),
            )
        );

        $ayat = new Ayat();
        $ayat->title = "Arouse";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>24,
                'ayat_number' =>"31",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-An-Nur',
                'title_english' =>'Lower their gaze and be modest, and to display of their adornment only that which is apparent, and to draw their veils over their bosoms',
                'title_urdu' =>' وہ اپنی نگاہیں نیچی رکھا کریں اور اپنی شرم گاہوں کی حفاظت کیا کریں اور اپنی آرائش (یعنی زیور کے مقامات) کو ظاہر نہ ہونے دیا کریں مگر جو ان میں سے کھلا رہتا ہو۔ اور اپنے سینوں پر اوڑھنیاں اوڑھے رہا کریں',
                'title_arabic' =>' يَغۡضُضۡنَ مِنۡ اَبۡصَارِهِنَّ وَيَحۡفَظۡنَ فُرُوۡجَهُنَّ وَلَا يُبۡدِيۡنَ زِيۡنَتَهُنَّ اِلَّا مَا ظَهَرَ مِنۡهَا‌ وَلۡيَـضۡرِبۡنَ بِخُمُرِهِنَّ عَلٰى جُيُوۡبِهِنّ',
                'description' => "Lowering the gaze and refraining from looking at that which Allah has forbidden. Allah says",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>12,
                'ayat_number' =>"33",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Yusuf',
                'title_english' =>'He said: O my Lord! Prison is dearer than that unto which they urge me, and if Thou fend not off their wiles from me I shall incline unto them and become of the foolish.',
                'title_urdu' =>'یوسف نے دعا کی کہ پروردگار جس کام کی طرف یہ مجھے بلاتی ہیں اس کی نسبت مجھے قید پسند ہے۔ اور اگر تو مجھ سے ان کے فریب کو نہ ہٹائے گا تو میں ان کی طرف مائل ہوجاؤں گا اور نادانوں میں داخل ہوجاؤں گا',
                'title_arabic' =>'قَالَ رَبِّ السِّجۡنُ اَحَبُّ اِلَىَّ مِمَّا يَدۡعُوۡنَنِىۡۤ اِلَيۡهِ‌ۚ وَاِلَّا تَصۡرِفۡ عَنِّىۡ كَيۡدَهُنَّ اَصۡبُ اِلَيۡهِنَّ وَاَكُنۡ مِّنَ الۡجٰهِلِيۡنَ',
                'description' => "Seek help by calling upon Allah and asking Him for help. The Quran tells us the lesson to be learned from the story of Yusuf (peace be upon him): ",
                'created_at' => now(),
            )
        );


        $ayat = new Ayat();
        $ayat->title = "Confused";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>3,
                'ayat_number' =>"159",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Al-Imran',
                'title_english' =>'And consult with them upon the conduct of affairs. And when thou art resolved, then put thy trust in Allah.',
                'title_urdu' =>'اور اپنے کاموں میں ان سے مشورت لیا کرو۔ اور جب (کسی کام کا) عزم مصمم کرلو تو خدا پر بھروسا رکھو۔',
                'title_arabic' =>'وَشَاوِرۡهُمۡ فِىۡ الۡاَمۡرِ‌ۚ فَاِذَا عَزَمۡتَ فَتَوَكَّلۡ عَلَى اللّٰ',
                'description' => "The first thing you should do is consult a reliable, pious and knowledgeable person, who you trust. Get their sound opinion regarding the matter at hand and consider their advice while making the final decision. For Allah has said:",
                'created_at' => now(),
            )
        );

        $ayat = new Ayat();
        $ayat->title = "Content";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>13,
                'ayat_number' =>"128",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-Ar-Ra'd",
                'title_english' =>'Verily in the remembrance of Allah do hearts find rest!',
                'title_urdu' =>'اور سن رکھو کہ خدا کی یاد سے دل آرام پاتے ہیں',
                'title_arabic' =>'اَلَا بِذِكۡرِ اللّٰهِ تَطۡمَٮِٕنُّ الۡقُلُوۡبُؕ',
                'description' => "It is the state of being happy and satisfied. Contentment is the state of being pleased and satisfied. No doubt, that the zikr of Allah is a source of comfort for mind and soul. Remembrance of Allah is a great worship. Qur’an stated that the hearts find rest in the Zikr of Allah.",
                'created_at' => now(),
            )
        );


        $ayat = new Ayat();
        $ayat->title = "Curious";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>49,
                'ayat_number' =>"12",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-Al-Hujurat",
                'title_english' =>'Shun much suspicion; for lo! some suspicion is a crime. And spy not, neither backbite one another. Would one of you love to eat the flesh of his dead brother? Ye abhor that (so abhor the other).',
                'title_urdu' =>'بہت گمان کرنے سے احتراز کرو کہ بعض گمان گناہ ہیں۔ اور ایک دوسرے کے حال کا تجسس نہ کیا کرو اور نہ کوئی کسی کی غیبت کرے۔ کیا تم میں سے کوئی اس بات کو پسند کرے گا کہ اپنے مرے ہوئے بھائی کا گوشت کھائے؟ اس (سے تو تم ضرور نفرت کرو گے۔ (تو غیبت نہ کرو',
                'title_arabic' =>'اجۡتَنِبُوۡا كَثِيۡرًا مِّنَ الظَّنِّ اِنَّ بَعۡضَ الظَّنِّ اِثۡمٌ‌ وَّلَا تَجَسَّسُوۡا وَلَا يَغۡتَبْ بَّعۡضُكُمۡ بَعۡضًا‌ؕ اَ يُحِبُّ اَحَدُكُمۡ اَنۡ يَّاۡكُلَ لَحۡمَ اَخِيۡهِ مَيۡتًا فَكَرِهۡتُمُوۡهُ',
                'description' => "Each and every one of us has our own set of faults and secrets that we do not wish the world to know – we want these secrets to be only between us and Allah (SWT). Hence, if Allah (SWT) is the concealer of our shortcomings and mistakes, why should we impose on others by prying into their private lives that have nothing to do with us. Prying usually leads to idle talk and jumping to conclusions, which may be completely false in reality. In the Holy Quran, Allah (SWT) warns man against these petty behaviors:",
                'created_at' => now(),
            )
        );


        $ayat = new Ayat();
        $ayat->title = "Defeated";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>2,
                'ayat_number' =>"153",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-Al-Baqarah",
                'title_english' =>'O ye who believe! Seek help in steadfastness and prayer. Lo! Allah is with the steadfast.',
                'title_urdu' =>'اے ایمان والو صبر اور نماز سے مدد لیا کرو',
                'title_arabic' =>'يٰٓاَيُّهَا الَّذِيۡنَ اٰمَنُوۡا اسۡتَعِيۡنُوۡا بِالصَّبۡرِ وَالصَّلٰوةِؕ ',
                'description' => "In the Qur’an it has been said multiple times that seek help with patience and Salat, Allah helps those who show patience:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>3,
                'ayat_number' =>"173",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-Al-Imran",
                'title_english' =>'Allah is Sufficient for us! Most Excellent is He in whom we trust!',
                'title_urdu' =>'ہم کو خدا کافی ہے اور وہ بہت اچھا کارساز ہے',
                'title_arabic' =>'حَسۡبُنَا اللّٰهُ وَنِعۡمَ الۡوَكِيۡلُ',
                'description' => "Dua for Success, victory and Allah’s help in all your affairs:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>3,
                'ayat_number' =>"139",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-Al-Imran",
                'title_english' =>'Faint not nor grieve, for ye will overcome them if ye are (indeed) believers.',
                'title_urdu' =>'اور (دیکھو) بے دل نہ ہونا اور نہ کسی طرح کا غم کرنا اگر تم مومن (صادق) ہو تو تم ہی غالب رہو گے',
                'title_arabic' =>'وَلَا تَهِنُوۡا وَ لَا تَحۡزَنُوۡا وَاَنۡتُمُ الۡاَعۡلَوۡنَ اِنۡ كُنۡتُمۡ مُّؤۡمِنِيۡنَ',
                'description' => null,
                'created_at' => now(),
            )
        );

        $ayat = new Ayat();
        $ayat->title = "Depressed";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>21,
                'ayat_number' =>"87",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Al-Anbya',
                'title_english' =>'There is no Allah save Thee. Be Thou Glorified! Lo! I have been a wrong-doer',
                'title_urdu' =>'تیرے سوا کوئی معبود نہیں۔ تو پاک ہے (اور) بےشک میں قصوروار ہوں',
                'title_arabic' =>'لَّآ إِلَـٰهَ إِلَّآ أَنتَ سُبْحَـٰنَكَ إِنِّى كُنتُ مِنَ ٱلظَّـٰلِمِينََ',
                'description' => "Here are duas that you may want to recite in facing depression:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>20,
                'ayat_number' =>"25-26",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Taha ',
                'title_english' =>'My Lord! Relieve my mind And ease my task for me',
                'title_urdu' =>'میرے پروردگار (اس کام کے لئے) میرا سینہ کھول دے اور میرا کام آسان کردے',
                'title_arabic' =>'رَبِّ اشۡرَحۡ لِىۡ صَدۡرِىۙ وَيَسِّرۡ لِىۡۤ اَمۡرِىۙ ',
                'description' => null,
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>60,
                'ayat_number' =>"4",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Taha ',
                'title_english' =>'Our Lord! In Thee we put our trust, and unto Thee we turn repentant, and unto Thee is the journeying.',
                'title_urdu' =>'اے ہمارے پروردگار تجھ ہی پر ہمارا بھروسہ ہے اور تیری ہی طرف ہم رجوع کرتے ہیں اور تیرے ہی حضور میں (ہمیں) لوٹ کر آنا ہے',
                'title_arabic' =>'رَّبَّنَا عَلَيۡكَ تَوَكَّلۡنَا وَاِلَيۡكَ اَنَـبۡنَا وَاِلَيۡكَ الۡمَصِيۡ',
                'description' => "Dua for attaining confidence in Allah SAW:",
                'created_at' => now(),
            )
        );

        $ayat = new Ayat();
        $ayat->title = "Desprate";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>28,
                'ayat_number' =>"24",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-An-Nur',
                'title_english' =>'My Lord! I am needy of whatever good Thou sendest down for me.',
                'title_urdu' =>'پروردگار میں اس کا محتاج ہوں کہ تو مجھ پر اپنی نعمت نازل فرمائے',
                'title_arabic' =>'رَبِّ اِنِّىۡ لِمَاۤ اَنۡزَلۡتَ اِلَىَّ مِنۡ خَيۡرٍ فَقِيۡ',
                'description' => "Many times we go through such hard times, that we become desperate and do or say irrational things and behave irresponsible. Let us first and foremost remember that in these cases Satan is in its highest peak. Best way to retaliate Satan is to remember Allah (SWT). Dua for desperation:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>13,
                'ayat_number' =>"28",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-Ar-Ra'd",
                'title_english' =>'Who have believed and whose hearts have rest in the remembrance of Allah. Verily in the remembrance of Allah do hearts find rest!',
                'title_urdu' =>'جو لوگ ایمان لاتے اور جن کے دل یادِ خدا سے آرام پاتے ہیں (ان کو) اور سن رکھو کہ خدا کی یاد سے دل آرام پاتے ہیں',
                'title_arabic' =>'الَّذِيۡنَ اٰمَنُوۡا وَتَطۡمَٮِٕنُّ قُلُوۡبُهُمۡ بِذِكۡرِ اللّٰهِ‌ؕ اَلَا بِذِكۡرِ اللّٰهِ تَطۡمَٮِٕنُّ الۡقُلُوۡبُؕ',
                'description' => "We as Muslims cannot deny the powerful connection between spirituality and mental well-being. Allah beautifully reveals the link between the two in a small ayah of The Quran:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>13,
                'ayat_number' =>"11",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-Ar-Ra'd",
                'title_english' =>'For him are angels ranged before him and behind him, who guard him by Allah\'s command. Lo! Allah change not the condition of a folk until they (first) change that which is in their hearts; and if Allah will misfortune for a folk there is none that can repel it, nor have they a defender beside Him.',
                'title_urdu' =>'اس کے آگے اور پیچھے خدا کے چوکیدار ہیں جو خدا کے حکم سے اس کی حفاظت کرتے ہیں۔ خدا اس (نعمت) کو جو کسی قوم کو (حاصل) ہے نہیں بدلتا جب تک کہ وہ اپنی حالت کو نہ بدلے۔ اور جب خدا کسی قوم کے ساتھ برائی کا ارادہ کرتا ہے تو پھر وہ پھر نہیں سکتی۔ اور خدا کے سوا ان کا کوئی مددگار نہیں ہوتا ',
                'title_arabic' =>'لَهٗ مُعَقِّبٰتٌ مِّنۡۢ بَيۡنِ يَدَيۡهِ وَمِنۡ خَلۡفِهٖ يَحۡفَظُوۡنَهٗ مِنۡ اَمۡرِ اللّٰهِ‌ؕ اِنَّ اللّٰهَ لَا يُغَيِّرُ مَا بِقَوۡمٍ حَتّٰى يُغَيِّرُوۡا مَا بِاَنۡفُسِهِمۡ‌ؕ وَاِذَاۤ اَرَادَ اللّٰهُ بِقَوۡمٍ سُوۡۤءًا فَلَا مَرَدَّ لَهٗ‌ۚ وَمَا لَهُمۡ مِّنۡ دُوۡنِهٖ مِنۡ وَّالٍ‏',
                'description' => "Once you have trained your mind to put things into perspective, your mind will start reasoning with your heart.  The remaining particles of fear of Allah in your heart will slowly prepare you to change. And put an end to the cycle of sin/ depression/ anxiety. Allah says in Quran:",
                'created_at' => now(),
            )
        );


        $ayat = new Ayat();
        $ayat->title = "Disbelieve";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>4,
                'ayat_number' =>"116",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-An-Nisa',
                'title_english' =>'Allah pardoneth not that partners should be ascribed unto Him. He pardoneth all save that to whom He will. Whoso ascribeth partners unto Allah hath wandered far astray.',
                'title_urdu' =>'خدا اس کے گناہ کو نہیں بخشے گا کہ کسی کو اس کا شریک بنایا جائے اور اس کے سوا (اور گناہ) جس کو چاہیے گا بخش دے گا۔ اور جس نے خدا کے ساتھ شریک بنایا وہ رستے سے دور جا پڑا',
                'title_arabic' =>'اِنَّ اللّٰهَ لَا يَغۡفِرُ اَنۡ يُّشۡرَكَ بِهٖ وَيَغۡفِرُ مَا دُوۡنَ ذٰلِكَ لِمَنۡ يَّشَآءُ‌ؕ وَمَنۡ يُّشۡرِكۡ بِاللّٰهِ فَقَدۡ ضَلَّ ضَلٰلاًۢ بَعِيۡدًا‏',
                'description' => "Disbelieve means Shirk with Allah. Shirk is the one unforgivable sin in Islam, if one dies in this state. Associating a partner or others with Allah is a rejection of Islam and takes one outside of the faith. Quran says:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>2,
                'ayat_number' =>"6",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Al-Baqarah',
                'title_english' =>'As for the Disbelievers, whether thou warn them or thou warn them not it is all one for them; they believe not.',
                'title_urdu' =>'جو لوگ کافر ہیں انہیں تم نصیحت کرو یا نہ کرو ان کے لیے برابر ہے۔ وہ ایمان نہیں لانے کے',
                'title_arabic' =>'اِنَّ الَّذِيۡنَ كَفَرُوۡا سَوَآءٌ عَلَيۡهِمۡ ءَاَنۡذَرۡتَهُمۡ اَمۡ لَمۡ تُنۡذِرۡهُمۡ لَا يُؤۡمِنُوۡن',
                'description' => null,
                'created_at' => now(),
            )
        );


        $ayat = new Ayat();
        $ayat->title = "Envious";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>113,
                'ayat_number' =>"5",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-AL-Falaq',
                'title_english' =>'And from the evil of the envier when he envieth.',
                'title_urdu' =>'اور حسد کرنے والے کی برائی سے جب حسد کرنے لگے',
                'title_arabic' =>'وَمِنۡ شَرِّ حَاسِدٍ اِذَا حَسَدَ',
                'description' => "Jealousy is a feeling of discomfort and resentment to the advantages and possessions of another person. It manifests as acts of disregard for the honor of others. When we’re jealous of a leader in our community for example, we backbite them at every chance hoping to erase their respect from people’s hearts. When we’re jealous of colleagues who enjoy a more successful career than us, we tell the world how unworthy they are of their position instead of celebrating their successes. We may be deceiving ourselves if we claim that we’re not jealous of anyone. Jealousy is so rampant that Allah (SWT) teaches us to seek refuge from its perpetrators by this Dua:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>3,
                'ayat_number' =>"26",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-AL-Falaq',
                'title_english' =>'Thou givest sovereignty unto whom Thou wilt, and Thou withdrawest sovereignty from whom Thou wilt. ',
                'title_urdu' =>'تو جس کو چاہے بادشاہی بخشے اور جس سے چاہے بادشاہی چھین',
                'title_arabic' =>'تُؤۡتِى الۡمُلۡكَ مَنۡ تَشَآءُ وَتَنۡزِعُ الۡمُلۡكَ مِمَّنۡ تَشَآءُ',
                'description' => "Allah who gives honor to those He wills.",
                'created_at' => now(),
            )
        );

        $ayat = new Ayat();
        $ayat->title = "Guilty";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>39,
                'ayat_number' =>"53",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Az-Zumar',
                'title_english' =>'who have been prodigal to their own hurt! Despair not of the mercy of Allah, Who forgives all sins.',
                'title_urdu' =>'جنہوں نے اپنی جانوں پر زیادتی کی ہے خدا کی رحمت سے ناامید نہ ہونا۔ خدا تو سب گناہوں کو بخش دیتا ہے ',
                'title_arabic' =>'لَا تَقۡنَطُوۡا مِنۡ رَّحۡمَةِ اللّٰهِ‌ؕ اِنَّ اللّٰهَ يَغۡفِرُ الذُّنُوۡبَ جَمِيۡعًا‌',
                'description' => "Mentioned as Al-Ghafoor (the most forgiving) in more than 90 places in the Glorious Qur’an, He offers every sinner chances of redemption and repentance. He says:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>7,
                'ayat_number' =>"55",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-Al-A'raf",
                'title_english' =>'Call upon your Lord humbly and in secret.',
                'title_urdu' =>'اپنے پروردگار سے عاجزی سے اور چپکے چپکے دعائیں مانگا کرو۔',
                'title_arabic' =>'اُدۡعُوۡا رَبَّكُمۡ تَضَرُّعًا وَّخُفۡيَةً‌ ؕ',
                'description' => "Pay a humble request  to Allah to retain our guilt:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>66,
                'ayat_number' =>"8",
                'book_name' =>'Quran',
                'surah_name' =>"Surah-At-Tahrim",
                'title_english' =>'Turn unto Allah in sincere repentance!',
                'title_urdu' =>' خدا کے آگے صاف دل سے توبہ کرو۔',
                'title_arabic' =>'تُوۡبُوۡۤا اِلَى اللّٰهِ تَوۡبَةً نَّصُوۡحًاؕ عَسٰى رَبُّكُمۡ',
                'description' => "For the one who believes in the mercy of the Almighty, guilt is a powerful means to direct his heart back to the one whose doors of tawbah (repentance) are always open. Allah says:",
                'created_at' => now(),
            )
        );

        $ayat = new Ayat();
        $ayat->title = "Nervous";
        $ayat->created_at = now();
        $ayat->save();
        $ayat->refresh();

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>21,
                'ayat_number' =>"87",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Al-Anbya',
                'title_english' =>'There is no Allah save Thee. Be Thou Glorified! Lo! I have been a wrong-doer',
                'title_urdu' =>'تیرے سوا کوئی معبود نہیں۔ تو پاک ہے (اور) بےشک میں قصوروار ہوں',
                'title_arabic' =>'لَّآ إِلَـٰهَ إِلَّآ أَنتَ سُبْحَـٰنَكَ إِنِّى كُنتُ مِنَ ٱلظَّـٰلِمِينََ',
                'description' => "Here are duas that you may want to recite in facing nervousness:",
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>20,
                'ayat_number' =>"25-26",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Taha ',
                'title_english' =>'My Lord! Relieve my mind And ease my task for me',
                'title_urdu' =>'میرے پروردگار (اس کام کے لئے) میرا سینہ کھول دے اور میرا کام آسان کردے',
                'title_arabic' =>'رَبِّ اشۡرَحۡ لِىۡ صَدۡرِىۙ وَيَسِّرۡ لِىۡۤ اَمۡرِىۙ ',
                'description' => null,
                'created_at' => now(),
            )
        );

        DB::table('ayat_details')->insert(array(
                'ayat_id' => $ayat->id,
                'surah_number' =>60,
                'ayat_number' =>"4",
                'book_name' =>'Quran',
                'surah_name' =>'Surah-Al-Mumtahanah ',
                'title_english' =>' Our Lord! In Thee we put our trust, and unto Thee we turn repentant, and unto Thee is the journeying',
                'title_urdu' =>'اے ہمارے پروردگار تجھ ہی پر ہمارا بھروسہ ہے اور تیری ہی طرف ہم رجوع کرتے ہیں اور تیرے ہی حضور میں (ہمیں) لوٹ کر آنا ہے',
                'title_arabic' =>'رَّبَّنَا عَلَيۡكَ تَوَكَّلۡنَا وَاِلَيۡكَ اَنَـبۡنَا وَاِلَيۡكَ الۡمَصِيۡ',
                'description' => "Dua for attaining confidence in Allah SAW:",
                'created_at' => now(),
            )
        );








    }
}
