<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Hadith;

class HadithSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hadith =  new Hadith();
        $hadith->title = "Anger";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 060,
                "hadith_number" => 3282,
                "title_english" => "Narrated Sulaiman bin Surd: While I was sitting in the company of the Prophet, two men abused each other and the face of one of them became red with anger, and his jugular veins swelled (i.e. he became furious). On that the Prophet said, I know a word, the saying of which will cause him to relax, if he does say it. If he says: 'I seek Refuge with Allah from Satan.' then all is anger will go away. Some body said to him, The Prophet has said, 'Seek refuge with Allah from Satan. ' The angry man said, Am I mad?",
                "title_urdu" =>'ہم سے عبدان نے بیان کیا، ان سے ابوحمزہ نے، ان سے اعمش نے، ان سے عدی بن ثابت نے اور ان سے سلیمان بن صرد رضی اللہ عنہ نے بیان کیا کہ   میں نبی کریم صلی اللہ علیہ وسلم کی خدمت میں بیٹھا ہوا تھا اور ( قریب ہی ) دو آدمی آپس میں گالی گلوچ کر رہے تھے کہ ایک شخص کا منہ سرخ ہو گیا اور گردن کی رگیں پھول گئیں۔ آپ صلی اللہ علیہ وسلم نے فرمایا کہ مجھے ایک ایسا کلمہ معلوم ہے کہ اگر یہ شخص اسے پڑھ لے تو اس کا غصہ جاتا رہے گا۔ فرمایا «أعوذ بالله من الشيطان‏.‏» ”میں پناہ مانگتا ہوں اللہ کی شیطان سے۔“ تو اس کا غصہ جاتا رہے گا۔ لوگوں نے اس پر اس سے کہا کہ نبی کریم صلی اللہ علیہ وسلم فرما رہے ہیں کہ تمہیں شیطان سے اللہ کی پناہ مانگنی چاہئے، اس نے کہا، کیا میں کوئی دیوانہ ہوں۔',
                "title_arabic" =>'حَدَّثَنَا عَبْدَانُ ، عَنْ أَبِي حَمْزَةَ ، عَنْ الْأَعْمَشِ ، عَنْ عَدِيِّ بْنِ ثَابِتٍ ، عَنْ سُلَيْمَانَ بْنِ صُرَدٍ ، قَالَ : كُنْتُ جَالِسًا مَعَ النَّبِيِّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ وَرَجُلَانِ يَسْتَبَّانِ فَأَحَدُهُمَا احْمَرَّ وَجْهُهُ وَانْتَفَخَتْ أَوْدَاجُهُ ، فَقَالَ النَّبِيُّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ : إِنِّي لَأَعْلَمُ كَلِمَةً لَوْ قَالَهَا ذَهَبَ عَنْهُ مَا يَجِدُ لَوْ ، قَالَ : أَعُوذُ بِاللَّهِ مِنَ الشَّيْطَانِ ذَهَبَ عَنْهُ مَا يَجِدُ ، فَقَالُوا : لَهُ إِنَّ النَّبِيَّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ ، قَالَ : تَعَوَّذْ بِاللَّهِ مِنَ الشَّيْطَانِ ، فَقَالَ : وَهَلْ بِي جُنُونٌ .ََ',
                "created_at" => now(),
            ]
        );

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sunan Abu Dawood",
                "book_number" => 043,
                "hadith_number" => 4784,
                "title_english" =>"When you are in anger you should do following as Hazrat Muhammad (SAW) says:
                “If any of you becomes angry and he is standing, let him sit down, so his anger will go away; if it does not go away, let him lie down.”",
                "title_urdu" =>"جب تم میں سے کسی کو غصہ آئے اور وہ کھڑا ہو تو چاہیئے کہ بیٹھ جائے، اب اگر اس کا غصہ رفع ہو جائے ( تو بہتر ہے ) ورنہ پھر لیٹ جائے",
                "title_arabic" =>"إِذَا غَضِبَ أَحَدُكُمْ وَهُوَ قَائِمٌ فَلْيَجْلِسْ، ‏‏‏‏‏‏فَإِنْ ذَهَبَ عَنْهُ الْغَضَبُ وَإِلَّا فَلْيَضْطَجِعْ",
                "created_at" => now(),
            ]
        );

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sunan Abu Dawood",
                "book_number" => 036,
                "hadith_number" => 4784,
                "title_english" =>"Hazrat Muhammad SAW says:
                “So when any of you is angry, he should perform ablution”",
                "title_urdu" =>"تم میں سے کسی کو جب غصہ آئے تووضو کر لے ۔",
                "title_arabic" =>"فَلْيَتَوَضَّأ  ‏‏‏‏‏‏فَإِذَا غَضِبَ أَحَدُكُم  ‏‏‏‏‏‏ْ‏‏‏‏‏‏ْ",
                "created_at" => now(),
            ]
        );

        $hadith =  new Hadith();
        $hadith->title = "Anxious";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 81,
                "hadith_number" => 6346,
                "title_english" =>"Hazrat Muhammad SAW prays to God when He is anxious:
                “There is no god but Allah, the Great, the Tolerant, there is no god but Allah, the Lord of the Magnificent Throne There is no god but Allah, the Lord of the Heaven and the earth, the Lord of the Edifying Throne.”",
                "title_urdu" =>"”اللہ صاحب عظمت اور بردبار کے سوا کوئی معبود نہیں، اللہ کے سوا کوئی معبود نہیں جو عرش عظیم کا رب ہے، اللہ کے سوا کوئی معبود نہیں جو آسمانوں اور زمینوں کا رب ہے اور عرش عظیم کا رب ہے۔“",
                "title_arabic" =>"لَا إِلَهَ إِلَّا اللَّهُ رَبُّ الْعَرْشِ الْعَظِيمِ ، لَا إِلَهَ إِلَّا اللَّهُ رَبُّ السَّمَوَاتِ وَرَبُّ الْأَرْضِ ، وَرَبُّ الْعَرْشِ الْكَرِيمِ ،‏‏‏‏‏‏ْ‏‏‏‏‏‏ْ",
                "created_at" => now(),
            ]
        );

        $hadith =  new Hadith();
        $hadith->title = "Arouse";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sunan An Nasai",
                "book_number" => 22,
                "hadith_number" => 2241,
                "title_english" =>"Hazrat Muhammad SAW says:
                “O young men, you should get married, for it is more effective in lowering the gaze and protecting one's chastity. Whoever cannot afford it should fast, for it will be a restraint Wija, for him.”",
                "title_urdu" =>"آپ صلی اللہ علیہ وسلم نے فرمایا: ”اے نوجوانوں کی جماعت! تم اپنے اوپر شادی کرنے کو لازم پکڑو کیونکہ یہ نظر کو نیچی اور شرمگاہ کو محفوظ رکھنے کا ذریعہ ہے، اور جو نہ کر سکتا ہو ( یعنی نان، نفقے کا بوجھ نہ اٹھا سکتا ہو ) وہ اپنے اوپر روزہ لازم کر لے کیونکہ یہ اس کے لیے بمنزلہ خصی بنا دینے کے ہے“۔",
                "title_arabic" =>" رَسُولِ اللَّهِ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ وَنَحْنُ شَبَابٌ لَا نَقْدِرُ عَلَى شَيْءٍ، ‏‏‏‏‏‏قَالَ:‏‏‏‏ يَا مَعْشَرَ الشَّبَابِ ! عَلَيْكُمْ بِالْبَاءَةِ، ‏‏‏‏‏‏فَإِنَّهُ أَغَضُّ لِلْبَصَرِ، ‏‏‏‏‏‏وَأَحْصَنُ لِلْفَرْجِ، ‏‏‏‏‏‏وَمَنْ لَمْ يَسْتَطِعْ فَعَلَيْهِ بِالصَّوْمِ، ‏‏‏‏‏‏فَإِنَّهُ لَهُ وِجَاءٌ .",
                "created_at" => now(),
            ]
        );

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 26,
                "hadith_number" => 1513,
                "title_english" =>"We should not stare others:
                “Narrated `Abdullah bin `Abbas: Al-Fadl (his brother) was riding behind Allah's Apostle and a woman from the tribe of Khath'am came and Al-Fadl started looking at her and she started looking at him. The Prophet turned Al-Fadl's face to the other side..”",
                "title_urdu" =>"عبداللہ بن عباس رضی اللہ عنہما نے بیان کیا کہ   فضل بن عباس ( حجۃ الوداع میں ) رسول اللہ صلی اللہ علیہ وسلم کے ساتھ سواری کے پیچھے بیٹھے ہوئے تھے کہ قبیلہ خثعم کی ایک خوبصورت عورت آئی۔ فضل اس کو دیکھنے لگے وہ بھی انہیں دیکھ رہی تھی۔ لیکن رسول اللہ صلی اللہ علیہ وسلم فضل رضی اللہ عنہ کا چہرہ بار بار دوسری طرف موڑ دینا چاہتے تھے",
                "title_arabic" =>"حَدَّثَنَا عَبْدُ اللَّهِ بْنُ يُوسُفَ ، أَخْبَرَنَا مَالِكٌ ، عَنِ ابْنِ شِهَابٍ ، عَنْ سُلَيْمَانَ بْنِ يَسَارٍ ، عَنْ عَبْدِ اللَّهِ بْنِ عَبَّاسٍ رَضِيَ اللَّهُ عَنْهُمَا , قَالَ : كَانَ الْفَضْلُ رَدِيفَ رَسُولِ اللَّهِ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ ، فَجَاءَتِ امْرَأَةٌ مِنْ خَثْعَمَ فَجَعَلَ الْفَضْلُ يَنْظُرُ إِلَيْهَا وَتَنْظُرُ إِلَيْهِ",
                "created_at" => now(),
            ]
        );

        $hadith =  new Hadith();
        $hadith->title = "Curious";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 79,
                "hadith_number" => 6066,
                "title_english" =>"Beware of suspicion, for suspicion is the worst of false tales. and do not look for the others' faults, and do not do spying on one another, and do not practice Najsh, and do not be jealous of one another and do not hate one another, and do not desert (stop talking to) one another. And O, Allah's worshipers! Be brothers! ",
                "title_urdu" =>"”بدگمانی سے بچتے رہو، بدگمانی اکثر تحقیق کے بعد جھوٹی بات ثابت ہوتی ہے اور کسی کے عیوب ڈھونڈنے کے پیچھے نہ پڑو، کسی کا عیب خواہ مخواہ مت ٹٹولو اور کسی کے بھاؤ پر بھاؤ نہ بڑھاؤ اور حسد نہ کرو، بغض نہ رکھو، کسی کی پیٹھ پیچھے برائی نہ کرو بلکہ سب اللہ کے بندے آپس میں بھائی بھائی بن کر رہو۔“",
                "title_arabic" =>" إِيَّاكُمْ وَالظَّنَّ فَإِنَّ الظَّنَّ أَكْذَبُ الْحَدِيثِ ، وَلَا تَحَسَّسُوا وَلَا تَجَسَّسُوا وَلَا تَنَاجَشُوا وَلَا تَحَاسَدُوا وَلَا تَبَاغَضُوا وَلَا تَدَابَرُوا وَكُونُوا عِبَادَ اللَّهِ إِخْوَانًا .",
                "created_at" => now(),
            ]
        );

        $hadith =  new Hadith();
        $hadith->title = "Depressed";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();


        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 81,
                "hadith_number" => 6346,
                "title_english" =>"Hazrat Muahmmad SAW prays to God when He is depressed:
                “There is no god but Allah, the Great, the Tolerant, there is no god but Allah, the Lord of the Magnificent Throne There is no god but Allah, the Lord of the Heaven and the earth, the Lord of the Edifying Throne.”",
                "title_urdu" =>"”اللہ صاحب عظمت اور بردبار کے سوا کوئی معبود نہیں، اللہ کے سوا کوئی معبود نہیں جو عرش عظیم کا رب ہے، اللہ کے سوا کوئی معبود نہیں جو آسمانوں اور زمینوں کا رب ہے اور عرش عظیم کا رب ہے۔“",
                "title_arabic" =>"لَا إِلَهَ إِلَّا اللَّهُ رَبُّ الْعَرْشِ الْعَظِيمِ ، لَا إِلَهَ إِلَّا اللَّهُ رَبُّ السَّمَوَاتِ وَرَبُّ الْأَرْضِ ، وَرَبُّ الْعَرْشِ الْكَرِيمِ ،‏‏‏‏‏‏ْ‏‏‏‏‏‏ْ",
                "created_at" => now(),
            ]
        );

        $hadith = new Hadith();
        $hadith->title = "Envious";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();


        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 79,
                "hadith_number" => 6064,
                "title_english" =>"No matter how high on Maslow’s hierarchy our needs may fall, He is able to fulfill them. When we realize His oneness in this manner, our fear and envy disappear:
                The Prophet said: do not be jealous of one another, and do not desert (cut your relation with) one another, and do not hate one another; and O Allah's worshipers! Be brothers (as Allah has ordered you!)",
                "title_urdu" =>"بی کریم صلی اللہ علیہ وسلم نے فرمایا آپس میں حسد نہ کرو، کسی کی پیٹھ پیچھے برائی نہ کرو، بغض نہ رکھو ۔“",
                "title_arabic" =>null,
                "created_at" => now(),
            ]
        );

        $hadith = new Hadith();
        $hadith->title = "Guilty";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();


        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Jami at Tirmidhi",
                "book_number" => 49,
                "hadith_number" => 3540,
                "title_english" =>"He protects His servants from the consequences of their sins, screens them and forgives them even though He is All-Knowing of the wrong they committed.
                Anas bin Malik narrated that the Messenger of Allah (ﷺ) said: “Allah, Blessed is He and Most High, said: ‘O son of Adam! Verily as long as you called upon me and hoped in Me, I forgave you, despite whatever may have occurred from you, and I did not mind. O son of Adam! Were your sins to reach the clouds of the sky, then you sought forgiveness from Me, I would forgive you, and I would not mind. So son of Adam! If you came to me with sins nearly as great as the earth, and then you met me not associating anything with Me, I would come to you with forgiveness nearly as great as it.”",
                "title_urdu" =>"انس بن مالک رضی الله عنہ کہتے ہیں کہ   میں نے رسول اللہ صلی اللہ علیہ وسلم کو فرماتے ہوئے سنا: ”اللہ کہتا ہے: اے آدم کے بیٹے! جب تک تو مجھ سے دعائیں کرتا رہے گا اور مجھ سے اپنی امیدیں اور توقعات وابستہ رکھے گا میں تجھے بخشتا رہوں گا، چاہے تیرے گناہ کسی بھی درجے پر پہنچے ہوئے ہوں، مجھے کسی بات کی پرواہ و ڈر نہیں ہے، اے آدم کے بیٹے! اگر تیرے گناہ آسمان کو چھونے لگیں پھر تو مجھ سے مغفرت طلب کرنے لگے تو میں تجھے بخش دوں گا اور مجھے کسی بات کی پرواہ نہ ہو گی۔ اے آدم کے بیٹے! اگر تو زمین برابر بھی گناہ کر بیٹھے اور پھر مجھ سے ( مغفرت طلب کرنے کے لیے ) ملے لیکن میرے ساتھ کسی طرح کا شرک نہ کیا ہو تو میں تیرے پاس اس کے برابر مغفرت لے کر آؤں گا ( اور تجھے بخش دوں گا ) “ امام ترمذی کہتے ہیں: یہ حدیث حسن غریب ہے اور ہم اس حدیث کو صرف اسی سند سے جانتے ہیں۔",
                "title_arabic" =>null,
                "created_at" => now(),
            ]
        );

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Bukhari",
                "book_number" => 49,
                "hadith_number" => 3540,
                "title_english" =>"Narrated Abu Huraira: Allah's Apostle (P.B.U.H) said, Our Lord, the Blessed, the Superior, comes every night down on the nearest Heaven to us when the last third of the night remains, saying: Is there anyone to invoke Me, so that I may respond to invocation? Is there anyone to ask me, so that I may grant him his request? Is there anyone seeking my forgiveness, so that I may forgive him?",
                "title_urdu" =>" رسول اللہ صلی اللہ علیہ وسلم نے فرمایا کہ ہمارا پروردگار بلند برکت والا ہے ہر رات کو اس وقت آسمان دنیا پر آتا ہے جب رات کا آخری تہائی حصہ رہ جاتا ہے۔ وہ کہتا ہے کوئی مجھ سے دعا کرنے والا ہے کہ میں اس کی دعا قبول کروں، کوئی مجھ سے مانگنے والا ہے کہ میں اسے دوں کوئی مجھ سے بخشش طلب کرنے والا ہے کہ میں اس کو بخش دوں",
                "title_arabic" =>null,
                "created_at" => now(),
            ]
        );

        $hadith = new Hadith();
        $hadith->title = "Nervous";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();


        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 81,
                "hadith_number" => 6346,
                "title_english" =>"Ibn 'Abbas reported that Allah's Apostle (ﷺ) used to supplicate during the time of trouble (in these words): ' There is no god but Allah, the Great, the Tolerant, there is no god but Allah, the Lord of the Magnificent Throne There is no god but Allah, the Lord of the Heaven and the earth, the Lord of the Edifying Throne.'",
                "title_urdu" =>"رسول اللہ صلی اللہ علیہ وسلم حالت پریشانی میں یہ دعا کیا کرتے تھے «لا إله إلا الله العظيم الحليم،‏‏‏‏ لا إله إلا الله رب العرش العظيم،‏‏‏‏ لا إله إلا الله رب السموات،‏‏‏‏ ورب الأرض،‏‏‏‏ ورب العرش الكريم» ”اللہ صاحب عظمت اور بردبار کے سوا کوئی معبود نہیں، اللہ کے سوا کوئی معبود نہیں جو عرش عظیم کا رب ہے، اللہ کے سوا کوئی معبود نہیں جو آسمانوں اور زمینوں کا رب ہے اور عرش عظیم کا رب ہے۔“‏",
                "title_arabic" =>null,
                "created_at" => now(),
            ]
        );

        $hadith = new Hadith();
        $hadith->title = "Lonely";
        $hadith->created_at = now();
        $hadith->save();
        $hadith->refresh();


        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 79,
                "hadith_number" => 6138,
                "title_english" =>"As Muslims, we can show others how to overcome loneliness by adhering to Islamic teachings and lifestyle. Keeping close contact with relatives remove loneliness.Hazrat Muhammad SAW says: Whoever believes in Allah and the Last Day, should unite the bond of kinship (i.e. keep good relation with his Kith and kin).",
                "title_urdu" =>"جو شخص اللہ اور آخرت کے دن پر ایمان رکھتا ہو اسے چاہئے کہ وہ صلہ رحمی کرے",
                "title_arabic" =>null,
                "created_at" => now(),
            ]
        );

        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 12,
                "hadith_number" => 929,
                "title_english" =>"Good gatherings would help one to overcome loneliness and would help us to be in constant touch with each other. Hazrat Muhammad SAW says:
                When it is a Friday, the angels stand at the gate of the mosque and keep on writing the names of the persons coming to the mosque in succession according to their arrivals. The example of the one who enters the mosque in the earliest hour is that of one offering a camel (in sacrifice). The one coming next is like one offering a cow and then a ram and then a chicken and then an egg respectively. When the Imam comes out (for Juma prayer) they (i.e. angels) fold their papers and listen to the Khutba.",
                "title_urdu" =>" جب جمعہ کا دن آتا ہے تو فرشتے جامع مسجد کے دروازے پر آنے والوں کے نام لکھتے ہیں، سب سے پہلے آنے والا اونٹ کی قربانی دینے والے کی طرح لکھا جاتا ہے۔ اس کے بعد آنے والا گائے کی قربانی دینے والے کی طرح پھر مینڈھے کی قربانی کا ثواب رہتا ہے۔ اس کے بعد مرغی کا، اس کے بعد انڈے کا۔ لیکن جب امام ( خطبہ دینے کے لیے ) باہر آ جاتا ہے تو یہ فرشتے اپنے دفاتر بند کر دیتے ہیں اور خطبہ سننے میں مشغول ہو جاتے ہیں۔",
                "title_arabic" =>null,
                "created_at" => now(),
            ]
        );


        DB::table('hadith_details')->insert(
            [
                "hadith_id"=> $hadith->id,
                "book_name" => "Sahih Bukhari",
                "book_number" => 12,
                "hadith_number" => 929,
                "title_english" =>"To keep mind engaged by teaching others about Islam help in loneliness’
                The Prophet said: By Allah! If a single person embraces Islam at your hands (i.e. through you), that will be better for you than the red camels.",
                "title_urdu" =>" آپ صلی اللہ علیہ وسلم نے فرمایا: اللہ کی قسم! اگر تمہارے ذریعہ ایک شخص کو بھی ہدایت مل جائے تو یہ تمہارے حق میں سرخ اونٹوں سے بہتر ہے۔",
                "title_arabic" =>null,
                "created_at" => now(),
            ]
        );





    }
}
