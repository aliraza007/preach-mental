<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeaderDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leader_details')->insert(array(
                'leader_id' => 1,
                'heading' => '',
                'paragraph' => 'Khalid ibn al-Walid ibn al-Mughira al-Makhzumi (Arabic: خالد بن الوليد بن المغيرة المخزومي‎, romanized: Khālid ibn al-Walīd ibn al-Mughīra al-Makhzūmī; died 642) was an Arab Muslim commander in the service of the Islamic prophet Muhammad and the caliphs Abu Bakr (r. 632–634) and Umar (r. 634–644) who played a leading role in the Ridda wars against rebel tribes in Arabia in 632–633 and the early Muslim conquests of Sasanian Iraq in 633–634 and Byzantine Syria in 634–638.

A horseman of the Quraysh tribe\'s aristocratic Makhzum clan, which ardently opposed Muhammad, Khalid played the instrumental role in defeating the Muslims at the Battle of Uhud in 625. Following his conversion to Islam in 627 or 629, he was made a commander by Muhammad, who bestowed on him the title Sayf Allah (the Sword of God). Khalid coordinated the safe withdrawal of Muslim troops during the abortive expedition to Mu\'ta against the Arab allies of the Byzantines in 629 and led the Bedouin contingents of the Muslim army during the capture of Mecca and the Battle of Hunayn in c. 630. After Muhammad\'s death, Khalid was appointed to suppress or subjugate Arab tribes in Najd and the Yamama (both regions in central Arabia) opposed to the nascent Muslim state, defeating the rebel leaders Tulayha at the Battle of Buzakha in 632 and Musaylima at the Battle of Aqraba in 633.

Khalid subsequently moved against the largely Christian Arab tribes and the Sasanian Persian garrisons of the Euphrates valley in Iraq. He was reassigned by Abu Bakr to command the Muslim armies in Syria and he led his men there on an unconventional march across a long, waterless stretch of the Syrian Desert, boosting his reputation as a military strategist. As a result of decisive victories against the Byzantines at Ajnadayn (634), Fahl (634), Damascus (634–635) and Yarmouk (636), the Muslims under Khalid conquered much of Syria. He was afterward demoted from the high command by Umar for a range of causes cited by traditional Islamic and modern sources. Khalid continued service as the key lieutenant of his successor Abu Ubayda ibn al-Jarrah in the sieges of Homs and Aleppo and the Battle of Qinnasrin, all in 637–638, which collectively precipitated the retreat from Syria of imperial Byzantine troops under Emperor Heraclius. Umar dismissed Khalid from his governorship of Qinnasrin afterward and he died in Medina or Homs in 642.
Khalid is generally considered by historians to be one of early Islam\'s most seasoned and accomplished generals and he is commemorated throughout the Arab world until the present day. The Islamic tradition credits Khalid for his battlefield tactics and effective leadership of the early Muslim conquests, but accuses him of illicitly executing Arab tribesmen who had accepted Islam, namely members of the Banu Jadhima during the lifetime of Muhammad and Malik ibn Nuwayra during the Ridda wars, and moral and fiscal misconduct in Syria. His military fame disturbed some of the pious, early Muslim converts, including Umar, who feared it could develop into a personality cult.
',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 1,
                'heading' => 'Conversion to Islam and service under Muhammad',
                'paragraph' => 'In the year 6 AH (c. 627) or 8 AH (c. 629) Khalid embraced Islam in Muhammad\'s presence alongside the Qurayshite Amr ibn al-As; the modern historian Michael Lecker comments that the accounts holding that Khalid and Amr converted in 8 AH are "perhaps more trustworthy".The historian Akram Diya Umari holds that Khalid and Amr embraced Islam and relocated to Medina following the Treaty of Hudaybiyya, apparently after the Quraysh dropped demands for the extradition of newer Muslim converts to Mecca. Following his conversion, Khalid "began to devote all his considerable military talents to the support of the new Muslim state", according to the historian Hugh N. Kennedy.

Khalid participated in the expedition to Mu\'ta in modern-day Jordan ordered by Muhammad in September 629. The purpose of the raid may have been to acquire booty in the wake of the Sasanian Persian army\'s retreat from Syria following its defeat by the Byzantine Empire in July. The Muslim detachment was routed by a Byzantine force consisting mostly of Arab tribesmen led by the Byzantine commander Theodore and several high-ranking Muslim commanders were slain. Khalid took command of the army following the deaths of the appointed commanders and, with considerable difficulty, oversaw a safe withdrawal of the Muslims. Muhammad rewarded Khalid by bestowing on him the honorary title Sayf Allah (Sword of God).
',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 1,
                'heading' => 'Battle of Yarmouk',
                'paragraph' => 'In the spring of 636, Khalid withdrew his forces from Damascus to the old Ghassanid capital at Jabiya in the Golan. He was prompted by the approach of a large Byzantine army dispatched by Heraclius, consisting of imperial troops led by Vahan and Theodore Trithyrius and frontier troops, including Christian Arab light cavalry led by the Ghassanid phylarch Jabala ibn al-Ayham and Armenian auxiliaries led by a certain Georgius (called Jaraja by the Arabs). The sizes of the forces cited by the medieval traditions are disputed by modern historians; Donner holds the Byzantines outnumbered the Muslims four to one, Walter E. Kaegi writes the Byzantines "probably enjoyed numerical superiority"with 15,000–20,000 or more troops,[12 and John Walter Jandora holds there was likely "near parity in numbers" between the two sides with the Muslims at 36,000 men (including 10,000 from Khalid\'s army) and the Byzantines at about 40,000. ',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 1,
                'heading' => 'Dismissal and death',
                'paragraph' => 'According to Sayf ibn Umar, later in 638 Khalid was rumored to have lavishly distributed war spoils from his northern Syrian campaigns, including a sum to the Kindite nobleman al-Ash\'ath ibn Qays. Umar consequently ordered that Abu Ubayda publicly interrogate and relieve Khalid from his post regardless of the interrogation\'s outcome, as well as to put Qinnasrin under Abu Ubayda\'s direct administration. Following his interrogation in Homs, Khalid issued successive farewell speeches to the troops in Qinnasrin and Homs before being summoned by Umar to Medina. There, Khalid complained to Umar that he treated him "like dirt", to which Umar responded by inquiring about the source of the wealth Khalid had accrued. Khalid clarified to Umar that the war booty was legally distributed among the Muslims and the caliph assured him that "you are truly an honorable man in my esteem, and you are dear to me; after today you will never have occasion to blame me". Sayf\'s account notes that Umar sent notice to the Muslim garrisons in Syria and Iraq that Khalid was dismissed not as a result of improprieties but because the troops had become "captivated by illusions on account of him [Khalid]" and he feared they would disproportionately place their trust in him rather than God. ',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 2,
                'heading' => '',
                'paragraph' => 'Muhammad bin Qasim al-Thaqafi (Arabic: محمد بن القاسم الثقفي‎, romanized: Muḥammad bin al-Qāsim al-Thaqafī; c. 695 – 715), also known by the laqab (honorific epithet) of Imad ad-Din (Arabic: عماد الدين‎, romanized: ʿImād al-Dīn), was an Arab military commander of the Umayyad Caliphate who led the Muslim conquest of Sindh and Multan from the last Hindu king, Raja Dahir in the battle of Aror. He was the first Muslim to have successfully captured Hindu territories and initiate the early Islamic India in 712 AD.


Information about Muhammad bin Qasim and the Arab conquest of Sindh in the medieval Arabic sources is limited compared to the contemporary Muslim conquest of Transoxiana. The Futuh al-Buldan (Conquests of the lands) by al-Baladhuri (d. 892) contains a few pages on the conquest of Sindh and Muhammad\'s forces, while biographical information is limited to a passage in the work of al-Ya\'qubi (d. 898), a few lines in the history of al-Tabari (d. 839) and scant mention in the Kitab al-aghani (Book of songs) of Abu al-Faraj al-Isfahani. A detailed account of Muhammad\'s conquest of Sind and his death is found in the Chach Nama, a 13th-century Persian text. The information in the Chach Nama purportedly derives from accounts by the descendants of the Arab soldiers of the 8th-century conquest, namely qadis (judges) and imams from the Sindhi cities of Alor and Bhakar who claimed descent from Muhammad\'s tribe, the Banu Thaqif. The orientalist Francesco Gabrieli holds the accounts likely emerged after c. 1000 and considers the Chach Nama to be a "historical romance" and "a late and doubtful source" for information about Muhammad.
',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 2,
                'heading' => 'Reasons for success',
                'paragraph' => 'Muhammad bin Qasim\'s success has been partly ascribed to Dahir being an unpopular Hindu king ruling over a Buddhist majority who saw Chach of Alor and his kin as usurpers of the Rai Dynasty. This is attributed to having resulted in support being provided by Buddhists and inclusion of rebel soldiers serving as valuable infantry in his cavalry-heavy force from the Jat and Meds. Brahman, Buddhist, Greek, and Arab testimony however can be found that attests towards amicable relations between the adherents of the two religions up to the 7th century.
Along with this were:
1.	Superior military equipment; such as siege engines and the Mongol bow.
2.	Troop discipline and leadership.
3.	The concept of Jihad as a morale booster.
4.	Religion; the widespread belief in the prophecy of Muslim success.
5.	The Samanis being persuaded to submit and not take up arms because the majority of the population was Buddhist who were dissatisfied with their rulers, who were Hindu.
6.	The laboring under disabilities of the Lohana Jats. Defections from among Dahirs chiefs and nobles.
',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 2,
                'heading' => 'Death',
                'paragraph' => 'There are two different accounts regarding the details of Muhammad\'s fate:
•	According to al-Baladhuri Muhammad was killed due to a family feud with the governor of Iraq. Sulayman was hostile toward Muhammad because apparently he had followed the order of Hajjaj to declare Sulayman\'s right of succession void in all territories conquered by him. When Muhammad received the news of the death of al-Hajjaj he returned to Aror. Muhammad was later arrested under the orders of the Caliph by the replacement governor of Sindh, Yazid ibn Abi Kabsha al-Saksaki, who worked under the new military governor of Iraq, Yazid ibn al-Muhallab, and the new fiscal governor, the mawla Salih ibn Abd al-Rahman. Salih, whose brother was executed by al-Hajjaj, tortured Muhammad and his relatives to death. The account of his death by al-Baladhuri is brief compared to the one in the Chach Nama.

The Chach Nama narrates a tale in which Muhammad\'s demise is attributed to the daughters of Dahir who had been taken captive during the campaign. Upon capture they had been sent on as presents to the Caliph for his harem in the capital Baghdad (however Baghdad had not yet been built and the actual capital was Damascus). The account relates that they then tricked the Caliph into believing that Muhammad had violated them before sending them on and as a result of this subterfuge, Muhammad was wrapped and stitched in oxen hides, and sent to Syria, which resulted in his death en route from suffocation. This narrative attributes their motive for this subterfuge to securing vengeance for their father\'s death. Upon discovering this subterfuge, the Caliph is recorded to have been filled with remorse and ordered the sisters buried alive in a wall.

',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 3,
                'heading' => '',
                'paragraph' => 'Harun al-Rashid (/hɑːˈruːn ɑːlrɑːˈʃiːd/; Arabic: هَارُون الرَشِيد‎ Hārūn Ar-Rašīd; "Aaron the Orthodox" or "Aaron the Righteous", 17 March 763 or February 766 – 24 March 809 (148–193 Hijri) was the fifth Abbasid Caliph. His birth date is debated, with various sources giving dates from 763 to 766. His epithet "al-Rashid" translates to "the Orthodox", "the Just", "the Upright", or "the Rightly-Guided". Harun ruled from 786 to 809, traditionally regarded to be the beginning of the Islamic Golden Age. He established the legendary library Bayt al-Hikma ("House of Wisdom") in Baghdad in present-day Iraq, and during his rule Baghdad began to flourish as a world center of knowledge, culture and trade. During his rule, the family of Barmakids, which played a deciding role in establishing the Abbasid Caliphate, declined gradually. In 796, he moved his court and government to Raqqa in present-day Syria.',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 3,
                'heading' => 'Caliphate',
                'paragraph' => 'Hārūn became caliph in 786 when he was in his early twenties. At the time, he was tall, good looking, and slim but strongly built, with wavy hair and olive skin. On the day of accession, his son al-Ma\'mun was born, and al-Amin some little time later: the latter was the son of Zubaida, a granddaughter of al-Mansur (founder of the city of Baghdad); so he took precedence over the former, whose mother was a Persian. Upon his accession, Harun led Friday prayers in Baghdad\'s Great Mosque and then sat publicly as officials and the layman alike lined up to swear allegiance and declare their happiness at his ascent to Amir al-Mu\'minin. He began his reign by appointing very able ministers, who carried on the work of the government so well that they greatly improved the condition of the people. Harun greatly admired the ancient Persian king Darius and, to a certain extent, attempted to imitate his rule.

Under Hārūn al-Rashīd\'s rule, Baghdad flourished into the most splendid city of its period. Tribute paid by many rulers to the caliph funded architecture, the arts and court luxuries.

',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 3,
                'heading' => 'Advisors',
                'paragraph' => 'A silver dirham minted in Madinat al-Salam (Bagdad) in 170 AH (786 CE). At the reverse, the inner marginal inscription says: "By order of the slave of God, Harun, Commander of the Faithful"
Hārūn was influenced by the will of his incredibly powerful mother in the governance of the empire until her death in 789. His vizier (chief minister) Yahya the Barmakid, Yahya\'s sons (especially Ja\'far ibn Yahya), and other Barmakids generally controlled the administration. The position of Persians in the Abbasid caliphal court reached its peak during al-Rashid\'s reign.
The Barmakids were a Persian family (from Balkh) that dated back to the Barmak, a hereditary Buddhist priest of Nava Vihara, who converted after the Islamic conquest of Balkh and became very powerful under al-Mahdi. Yahya had helped Hārūn to obtain the caliphate, and he and his sons were in high favor until 798, when the caliph threw them in prison and confiscated their land. Muhammad ibn Jarir al-Tabari dates this event to 803 and lists various reasons for it: Yahya\'s entering the Caliph\'s presence without permission; Yahya\'s opposition to Muhammad ibn al Layth, who later gained Harun\'s favour; and Ja\'far\'s release of Yahya ibn Abdallah ibn Hasan, whom Harun had imprisoned.
',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 3,
                'heading' => 'Rebellions',
                'paragraph' => 'Because of the Thousand and One Nights tales, Harun al-Rashid turned into a legendary figure obscuring his true historic personality. In fact, his reign initiated the political disintegration of the Abbasid caliphate. Syria was inhabited by tribes with Umayyad sympathies and remained the bitter enemy of the Abbasids, while Egypt witnessed uprisings against Abbasids due to maladministration and arbitrary taxation. The Umayyads had been established in Spain in 755, the Idrisids in Morocco in 788, and the Aghlabids in Ifriqiya (modern Tunisia) in 800. Besides, unrest flared up in Yemen, and the Kharijites rose in rebellion in Daylam, Kerman, Fars and Sistan. Revolts also broke out in Khorasan, and al-Rashid waged many campaigns against the Byzantines.',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 3,
                'heading' => 'Death',
                'paragraph' => 'A major revolt led by Rafi ibn al-Layth was started in Samarqand which forced Harun al-Rashid to move to Khorasan. He first removed and arrested Ali bin Isa bin Mahan but the revolt continued unchecked. Harun al-Rashid became ill and died very soon after when he reached Sanabad village in Tus and was buried in Dar al-Imarah, the summer palace of Humayd ibn Qahtaba, the Abbasid governor of Khorasan. Due to this historical event, the Dar al-Imarah was known as the Mausoleum of Haruniyyeh. The location later became known as Mashhad ("The Place of Martyrdom") because of the martyrdom of Imam al-Ridha in 818.

',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 4,
                'heading' => '',
                'paragraph' => 'Mahmud of Ghazni (Persian: محمود غزنوی‎; 2 November 971 – 30 April 1030) was the first independent ruler of the Turkic dynasty of Ghaznavids, ruling from 999 to 1030. At the time of his death, his kingdom had been transformed into an extensive military empire, which extended from northwestern Iran proper to the Punjab in the Indian subcontinent, Khwarazm in Transoxiana, and Makran.

',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 4,
                'heading' => 'Background',
                'paragraph' => 'Mahmud was born in the town of Ghazni in the region of Zabulistan (now present-day Afghanistan) on 2 November 971. His father, Sabuktigin, was a Turkic slave commander who laid foundations to the Ghaznavid dynasty in Ghazni in 977, which he ruled as a subordinate of the Samanids, who ruled Khorasan and Transoxiana. Mahmud\'s mother was the daughter of an Iranian aristocrat from Zabulistan, and is therefore known in some sources as Mahmud-i Zavuli ("Mahmud from Zabulistan"). Not much about Mahmud\'s early life is known, other than that he was a school-fellow of Ahmad Maymandi, a Persian native of Zabulistan and foster brother of his.
',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 4,
                'heading' => 'Personality',
                'paragraph' => 'Sultan Mahmud thought of himself as "the Shadow of the God on Earth", an absolute power whose will is law. He paid great attention to details in almost everything, personally overseeing the work of every department of his diwan (administration). He watched closely over the activities of the highest statesmen of his empire, especially his military commanders as he would not tolerate any mistreatment with regard to ordinary people.[citation needed]
Mahmud appointed all his ministers himself without advising his wazir (chief advisor) or diwan, though occasionally he had to, as his religion dictated that Muslims should consult each other on all issues. Most of the time he was suspicious of his ministers, particularly of the wazir, and the following words are widely believed to be his: "wazirs are the enemies of kings..." Sultan Mahmud had numerous spies (called mushrifs) across his empire, supervised by the special department within his diwan.


',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 4,
                'heading' => 'Attack on the Somnath Temple',
                'paragraph' => 'In 1025 Mahmud raided Gujarat, plundering the Somnath temple and breaking its jyotirlinga. He took away booty of 2 million dinars. The conquest of Somnath was followed by a punitive invasion of Anhilwara. Some historians claim that there are records of pilgrimages to the temple in 1038 that do not mention damage to the temple. However, powerful legends with intricate detail had developed regarding Mahmud\'s raid in the Turko-Persian literature, which "electrified" the Muslim world according to scholar Meenakshi Jain.
',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 4,
                'heading' => 'Political challenges',
                'paragraph' => 'The last four years of Mahmud\'s life were spent contending with the influx of Oghuz and Seljuk Turks from Central Asia and the Buyid dynasty. Initially, after being repulsed by Mahmud, the Seljuks retired to Khwarezm, but Togrül and Çagrı led them to capture Merv and Nishapur (1028–1029). Later, they repeatedly raided and traded territory with his successors across Khorasan and Balkh and even sacked Ghazni in 1037. In 1040, at the Battle of Dandanaqan, they decisively defeated Mahmud\'s son, Mas\'ud I, resulting in Mas\'ud abandoning most of his western territories to the Seljuks.

',
                'created_at' => now(),
            )
        );
        DB::table('leader_details')->insert(array(
                'leader_id' => 4,
                'heading' => 'Death',
                'paragraph' => 'Sultan Mahmud died on 30 April 1030. His mausoleum is located in Ghazni, Afghanistan.',
                'created_at' => now(),
            )
        );
    }
}
