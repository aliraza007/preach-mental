<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            'name' => 'Admin',
            'surname'=>'ali',
            'email' => 'admin@yopmail.com',
            'pin_code' => 83379,
            'unique_code' => "ADMIN12wewe",
            'password' => Hash::make('123456'),
            'mobile_number'=>'+923063730499',
            'avatar_src'=>'',
            'sign_in_count'=>0,
            'status_id'=>1,
            'notification_enabled'=>0,
            'biometric_enabled'=>0,
            'is_social'=>0,
            'created_at' => now(),
            )
        );
        DB::table('users')->insert(array(
            'name' => 'ali',
            'surname'=>'ali',
            'email' => 'ali@yopmail.com',
            'pin_code' => 11111,
            'unique_code' => "212121212",
            'password' => Hash::make('123456'),
            'mobile_number'=>'+923063730499',
            'avatar_src'=>'',
            'sign_in_count'=>0,
            'status_id'=>1,
            'notification_enabled'=>0,
            'biometric_enabled'=>0,
            'is_social'=>0,
            'created_at' => now(),
            )
        );

        DB::table('users')->insert(array(
            'name' => 'shah',
            'surname'=>'shah',
            'email' => 'shah@yopmail.com',
            'pin_code' => 22222,
            'unique_code' => "313131313",
            'password' => Hash::make('123456'),
            'mobile_number'=>'+923063730499',
            'avatar_src'=>'',
            'sign_in_count'=>0,
            'status_id'=>1,
            'notification_enabled'=>0,
            'biometric_enabled'=>0,
            'is_social'=>0,
            'created_at' => now(),
            )
        );

        DB::table('user_roles')->insert(array(
            'role_id' =>1,
            'user_id'=>1,
            'created_at' => now(),

            )
        );

        DB::table('user_roles')->insert(array(
            'role_id' =>2,
            'user_id'=>2,
            'created_at' => now(),
            )
        );

        DB::table('user_roles')->insert(array(
            'role_id' =>2,
            'user_id'=>2,
            'created_at' => now(),
            )
        );

    }
}
