<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(HadithSeeder::class);
        $this->call(AyatSeeder::class);
        $this->call(LeaderSeeder::class);
        $this->call(LeaderDetailSeeder::class);
    }
}
