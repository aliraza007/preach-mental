<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leaders')->insert(array(
                'name' => 'Khalid ibn al-Walid',
                'created_at' => now(),
            )
        );
        DB::table('leaders')->insert(array(
                'name' => 'Harun al-Rashid',
                'created_at' => now(),
            )
        );
        DB::table('leaders')->insert(array(
                'name' => 'Mahmud of Ghazni',
                'created_at' => now(),
            )
        );
        DB::table('leaders')->insert(array(
                'name' => 'Babur',
                'created_at' => now(),
            )
        );
        //
    }
}
