<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('status', 255);
            $table->text('description')->nullable();
            $table->timestamps();
        });

        $now = Carbon::now();
        DB::table('statuses')->insert([
            [
                'id' => 1,
                'status' => 'Active',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 2,
                'status' => 'Pending',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 3,
                'status' => 'Suspended',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 4,
                'status' => 'Ready',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 5,
                'status' => 'Done',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 6,
                'status' => 'Submitted',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 7,
                'status' => 'Occupied',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 8,
                'status' => 'Progress',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 9,
                'status' => 'Completed',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 10,
                'status' => 'Cancelled',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 11,
                'status' => 'Available',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 12,
                'status' => 'Reserved',
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
