<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('name', 255);
            $table->string('surname', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('password', 255)->nullable();
            $table->string('pin_code', 10)->unique()->nullable();
            $table->string('unique_code', 255)->unique();
            $table->string('avatar_src', 500)->default('default_logo.png')->nullable();
            $table->string('mobile_number', 255)->nullable();;
            $table->unsignedInteger('sign_in_count')->default(0);
            $table->timestamp('last_active_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('is_loggedin')->default(false);
            $table->string('remember_token', 255)->nullable();
            $table->text('forgot_code')->nullable();
            $table->dateTime('forgot_code_created_at')->nullable();
            $table->foreignId('status_id')->default(1)
                ->constrained('statuses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->boolean('notification_enabled')->default(true);
            $table->boolean('biometric_enabled')->default(false);
            $table->boolean('is_social')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
