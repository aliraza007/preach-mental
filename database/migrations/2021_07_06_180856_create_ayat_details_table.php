<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAyatDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ayat_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ayat_id')
                ->constrained('ayats')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('ayat_number')->nullable();
            $table->text('surah_name')->nullable();
            $table->integer('surah_number')->nullable();
            $table->text('book_name')->nullable();
            $table->text('book_number')->nullable();
            $table->integer('volume_number')->nullable();
            $table->longText('title_arabic')->nullable();
            $table->longText('title_urdu')->nullable();
            $table->longText('title_english')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ayat_details');
    }
}
