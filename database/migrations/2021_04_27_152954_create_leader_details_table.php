<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leader_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('leader_id')
                ->constrained('leaders')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('heading')->nullable();
            $table->longText('paragraph')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leader_details');
    }
}
