<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHadithDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hadith_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('hadith_id')
                ->constrained('hadiths')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->integer('hadith_number')->nullable();
            $table->string('book_name')->nullable();
            $table->integer('book_number')->nullable();
            $table->integer('volume_number')->nullable();
            $table->string('reference_name')->nullable();
            $table->longText('title_arabic')->nullable();
            $table->longText('title_urdu')->nullable();
            $table->longText('title_english')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hadith_details');
    }
}
