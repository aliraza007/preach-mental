<?php
namespace App\Core\Traits;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Mail;


trait ResponseTrait {
    /**
     * @param array|null $payload
     * @param int|null $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse(array $payload=null, int $statusCode=null)
    {
        return response()->json($payload, $statusCode);
    }

    /**
     * @param string $message
     * @param array|null $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function error(string $message, array $data = null, $statusCode=400) {
        $statusCode = ($statusCode !== 0 ? $statusCode : 400);

        return $this->jsonResponse([
            'message' => $message,
            'status' => $statusCode,
            'data' => $data
        ], $statusCode);
    }

    /**
     * @param null $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function validationError($errors = null) {
        $statusCode =  422;

        return $this->jsonResponse([
            'message' => null,
            'errors' => $errors,
            'status' => $statusCode,
            'data' => null,
        ], $statusCode);
    }

    public function responseGet(string $message, $data = null, $metadata = null, $statusCode = 200) {
        /* If a collection hasn't been passed, show everything */
        if(!($data instanceof ResourceCollection)) {
            return $this->jsonResponse(
                [
                    'message' => $message,
                    'status' => $statusCode,
                    'data' => $data,
                ],
                $statusCode
            );
        }

        /* If a collection has been passed, the data will already be in there. Just add on any additional fields */
        return $data->additional(
            array_merge(
                $data->additional,
                [
                    'message' => $message,
                    'status' => $statusCode,
                    'meta' => [
                        'custom' => $metadata
                    ],
                ]
            )
        );
    }

    /* Scenario specific */

    protected function notFound($message = 'Resource not found') {
        return $this->error($message, null, 404);
    }

    protected function forbiddenRequest($message = 'You do not have permission to access this resource') {
        return $this->error($message, null, 403);

    }

    protected function badRequest($message = null) {
        return $this->error($message, null, 400);
    }

    public function sendEmail(string $fromEmail=null,string $fromName,array $data=null,string $templates=null,$subject=null)
    {
        try {
            $senderEmail = config("mail.mailers.smtp.username");
//            dd($senderEmail);
//            $senderEmail = env("MAIL_USERNAME", "rapidzztest@gmail.com");
            $senderName = "Preach Mental® Support";
            if(empty($subject)){
                $subject = "Mail";
            }

            Mail::send($templates,['data'=> $data], function ($message) use ($fromEmail,$fromName,$subject,$senderEmail,$senderName) {
                $message->to($fromEmail, $fromName)->subject($subject);
                $message->from($senderEmail,$senderName);
            });
        } catch(\Exception $e){
            throw new \Exception($e->getMessage());
//            throw new \Exception('Failed to send mail!');
        }

    }


}
