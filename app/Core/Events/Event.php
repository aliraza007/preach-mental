<?php

namespace  App\Core\Events;

use Illuminate\Queue\SerializesModels;

class Event
{
    use SerializesModels;
}
