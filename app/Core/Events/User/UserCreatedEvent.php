<?php

namespace App\Core\Events\User;

use App\Models\User;
use App\Core\Events\Event;


class UserCreatedEvent extends Event
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
