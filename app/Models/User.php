<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;

    //For Authentication
    protected $_token;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'password', 'mobile_number', 'biometric_enabled', 'notification_enabled', 'avatar_src'
    ];

    protected $appends = ['token', 'avatar'];

    protected $dates = ['created_at', 'updated_at', 'last_active_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id', 'password', 'deleted_at', 'pin_code', 'status_id'];

    protected $softCascade = ['consumerProfile'];
    /**
     * @var array
     */
    private $dontKeepRevisionOf = ['last_active_at'];

    public function loadDetails()
    {
        $this->load(['devices']);
        return $this;
    }


    /**
     * user devices
     */
    public function devices()
    {
        return $this->hasMany(PushToken::class);
    }


    /**
     * @return array
     */
    public function routeNotificationForFcm()
    {
        return $this->devices()->pluck('device', 'token')->toArray();
    }

    /**
     * user status
     */
    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    /**
     * @return mixed
     */
    public function getTokenAttribute()
    {
        return $this->_token;
    }

    /**
     * @param $value
     */
    public function setTokenAttribute($value)
    {
        $this->_token = $value;
    }


    /**
     * This returns a share code that is 10 characters in length
     * based on the first 6 characters of their name and surname
     * together, the other 4 characters are filled in from a hash
     * of their id.
     * @return string
     */
    public function generateUniqueCode()
    {
        $name = Str::slug($this->name);
        $firstPart = strtolower(substr(Str::camel(Str::slug($name)), 0, 6));
        $secondPart = strtolower(substr(Str::camel(Str::slug(Hash::make($this->id))), -10));

        $uniqueCode = substr($firstPart . $secondPart, 0, 10);

        return strtoupper($uniqueCode);
    }

    /**
     * @return |null
     */
    public function getAvatarAttribute()
    {
        try {
            if (!empty($avatar = $this->avatarFile)) {
                return $avatar->getThumbCached(400, 400);
            }
        } catch (\Exception $e) {
        }

        return null;
    }

    /**
     * @param User $user
     * @param null $device
     * @return bool
     */
    public static function updateDevice(User $user, $device = null)
    {
        if (!empty($device)) {
            if ($user->hasDevice()) {
                if ($user->devices()->update($device))
                    return true;
            } else {
                if ($user->devices()->create($device))
                    return true;
            }
        }
        return false;
    }


    // Social users
    public function social()
    {
        return $this->hasMany(SocialUser::class, 'user_id', 'id');
    }

    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }


    /**
     * @return boolean
     */
    public function hasDevice()
    {
        return $this->devices()->exists();
    }

}

