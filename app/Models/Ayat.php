<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ayat extends Model
{
    use HasFactory,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'search_title'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function ayat_detail()
    {
        return $this->hasMany(AyatDetail::class, 'ayat_id', 'id');
    }



}
