<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leader extends Model
{
    use HasFactory;
    public function getName()
    {
        return $this->name;
    }
    public function getUrduName()
    {
        return $this->urdu_name;
    }
    public function getArabicName()
    {
        return $this->arabic_name;
    }
    public function getEnglishName()
    {
        return $this->english_name;
    }

    public function details()
    {
        return $this->hasMany(LeaderDetail::class,'leader_id','id');
    }



}
