<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AyatDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        "ayat_number","surah_name","surah_number","book_name","book_number","volume_number","title_arabic","title_urdu","title_english","description"
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function ayat()
    {
        return $this->belongsTo(Ayat::class, 'ayat_id', 'id');
    }
}
