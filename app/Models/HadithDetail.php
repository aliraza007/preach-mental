<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HadithDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        "hadith_number","book_name","book_number","volume_number","reference_name","title_arabic","title_urdu","title_english","description"
    ];


    protected $dates = ['created_at', 'updated_at'];

    public function hadith()
    {
        return $this->belongsTo(Hadith::class, 'hadith_id', 'id');
    }
}
