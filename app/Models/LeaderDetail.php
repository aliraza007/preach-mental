<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaderDetail extends Model
{
    use HasFactory;

    public function leader()
    {
        return $this->belongsTo(Leader::class,'leader_id','id');
    }
}
