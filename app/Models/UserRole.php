<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;

class UserRole extends Model
{
    use HasFactory;

    protected $table = "user_roles";

    protected $fillable = [''];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

}
