<?php


namespace App\QueryFilters;


interface Criteria
{
    public function apply($query);
}