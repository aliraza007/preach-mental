<?php

namespace App\QueryFilters;
use Illuminate\Support\Arr;
abstract class CriteriaAbstract
{
    protected $value;
    protected $relationPath;

    /**
     * Entity map to map relations.
     */
    static public $relationMap = [];

    public function __construct(string $queryOrigin = null, $value)
    {
        $this->relationPath = $this->getRelationPath($queryOrigin);
        $this->value = $value;
    }

    public function apply($query){
        $value = $this->value;
        $extra = $this->getExtra();

        if($this->relationPath === null){
            $this->applyCriteria($query, $value, $extra);
            return;
        }
        $query->whereHas($this->relationPath, function($query) use($value, $extra) {
            $this->applyCriteria($query, $value, $extra);
        });
    }

    public function applyCriteria($query, $value, $extra = null){
        // Add your conditions here
    }

    public function getExtra(){
        // Add your extra here
    }

    public function getRelationPath($queryOrigin){
        return Arr::get(static::$relationMap, $queryOrigin, null);
    }
}