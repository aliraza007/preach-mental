<?php

namespace App\QueryFilters;

use Illuminate\Database\Eloquent\Builder;
use App\QueryFilters\Criteria;
use App\QueryFilters\CriteriaAbstract;
use App\QueryFilters\Order;

class Query
{
    /**
     * @var mixed
     */
    protected $baseQuery;

    /**
     * @var Criteria[]
     */
    protected $criteria;

    /**
     * @var Order[]
     */
    protected $orders;

    /**
     * @var int
     */
    protected $page;

    /**
     * @var int
     */
    protected $perPage;

    /**
     * Query constructor.
     */
    public function __construct()
    {
        $this->criteria = [];
        $this->orders = [];
        $this->page = -1;
        $this->perPage = -1;
        $this->baseQuery = null;
    }

    /**
     * @param Criteria $criteria
     * @return \Injector\Api\Companies\Repositories\Queries\Query
     */
    public function addCriteria(Criteria $criteria)
    {
        $this->criteria[] = $criteria;
        return $this;
    }

    /**
     * @param CriteriaAbstract $criteria
     * @return $this
     */
    public function addNewCriteria(CriteriaAbstract $criteria)
    {
        $this->criteria[] = $criteria;
        return $this;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function addOrder(Order $order)
    {
        $this->orders[] = $order;
        return $this;
    }

    /**
     * @param int $page
     * @param int $perPage
     * @return $this
     * @throws \Exception
     */
    public function setPagination($page, $perPage)
    {
        $this->page = $page;
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @param $query
     * @return $this
     */
    public function setQuery($query)
    {
        $this->baseQuery = $query;
        return $this;
    }

    /**
     * @return Criteria[]
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @return Order[]
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Build query
     *
     * @return mixed|Builder
     * @throws \Exception
     */
    public function build()
    {
        if($this->baseQuery === null) {
            throw new \Exception('Query is not set');
        }

        $query = clone $this->baseQuery;

        foreach($this->criteria as $criteria) {
            $criteria->apply($query);
        }

        foreach($this->orders as $order) {
            $order->apply($query);
        }

        return $query;
    }

    /**
     * Paginate
     *
     * @return mixed
     * @throws \Exception
     */
    public function paginate()
    {
        $query = $this->build();
        return $query->paginate($this->perPage, ['*'], 'page', $this->page);
    }

    /**
     * Get all items
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws \Exception
     */
    public function all()
    {
        $query = $this->build();
        return $query->get();
    }

    /**
     * Get one item
     *
     * @return mixed
     * @throws \Exception
     */
    public function first()
    {
        $query = $this->build();
        return $query->first();
    }
}