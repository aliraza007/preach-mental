<?php

namespace App\QueryFilters;

use Illuminate\Database\Eloquent\Builder;

class Order
{
    const ASC = 'asc';
    const DESC = 'desc';

    /**
     * @var $column
     */
    private $column;

    /**
     * @var $direction
     */
    private $direction;

    /**
     * Order constructor.
     * @param string $column
     * @param string $direction
     */
    public function __construct(string $column, string $direction)
    {
        $this->column = $column;
        $this->direction = $direction;
    }

    /**
     * @param mixed|Builder $query
     * @return $this
     * @throws \Exception
     */
    public function apply($query)
    {
        if ($this->direction === static::ASC || $this->direction === static::DESC) {
            $query->orderBy($this->column, $this->direction);

            return $this;
        } else {
            throw new \Exception('Invalid Order Direction, must be ASC or DESC');
        }
    }
}
