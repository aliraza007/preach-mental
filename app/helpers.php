<?php
/**
* Brought by https://gist.github.com/mabasic/21d13eab12462e596120
*/
if (!function_exists('config_path')) {
    /**
    * Get the configuration path.
    *
    * @param  string $path
    * @return string
    */
    function config_path($path = '') {
        return app()->basePath().DIRECTORY_SEPARATOR.'config' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if(!function_exists('get_sql')) {
    function get_sql($query) {
        return str_replace("''", "'", vsprintf(str_replace("?", "'%s'", $query->toSql()), $query->getBindings()));
    }
}

if(!function_exists('get_log_sql')) {
    function get_log_sql($log) {
        $formattedQueries = [];
        foreach( $log as $query ) {
            $time = $query['time'] / 1000;
            $prep = $query['query'];
            foreach ($query['bindings'] as $binding) {
                $value = $binding;

                if($binding instanceof DateTime) {
                    $value = "'" . $binding->format('Y-m-d H:i:s') . "'";
                } elseif (is_string($binding)) {
                    $value = "'" . $binding . "'";
                } elseif (is_bool($binding)) {
                    $value = $binding ? 'TRUE' : 'FALSE';
                }

                $prep = preg_replace("#\?#", $value, $prep, 1);
            }
            $formattedQueries[] = [
                'query' => $prep,
                'time' => $time
            ];
        }
        return $formattedQueries;
    }
}

if(!function_exists('geo_distance')) {
    function geo_distance($lat1, $lon1, $lat2, $lon2, $unit = "M") {
        if(!(is_numeric($lat1) && is_numeric($lon1) && is_numeric($lat2) && is_numeric($lon2))) {
            //Invalid
            return 0;
        }

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}

if(!function_exists('geo_distance_sql')) {
    function geo_distance_sql($lat1, $lng1, $lat2, $lng2, $metric = 'mi') {
        return "( IF('$metric' = 'km', 6371, 3959) * acos( cos( radians($lat2) ) * cos( radians( $lat1 ) ) * cos( radians( $lng1 ) - radians($lng2) ) + sin( radians($lat2) ) * sin( radians( $lat1 ) ) ) )";
    }
}


if (!function_exists('app_path')) {
    /**
     * Get the path to the application folder.
     *
     * @param  string $path
     * @return string
     */
    function app_path($path = '')
    {
        return app('path') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if(class_exists(Polyline::class)) {
    function get_google_maps_circle($Lat, $Lng, $Rad, $Detail = 9)
    {
        $R = 6371;
        $pi = pi();
        $Lat = ($Lat * $pi) / 180;
        $Lng = ($Lng * $pi) / 180;
        $d = $Rad / $R;
        $points = array();
        for ($i = 0; $i <= 360; $i += $Detail) {
            $brng = $i * $pi / 180;
            $pLat = asin(sin($Lat) * cos($d) + cos($Lat) * sin($d) * cos($brng));
            $pLng = (($Lng + atan2(sin($brng) * sin($d) * cos($Lat), cos($d) - sin($Lat) * sin($pLat))) * 180) / $pi;
            $pLat = ($pLat * 180) / $pi;
            $points[] = array($pLat, $pLng);
        }

        return Polyline::encode($points);
    }

    if (!function_exists('array_key_first')) {
        function array_key_first(array $array)
        {
            foreach ($array as $key => $value) {
                return $key;
            }
        }
    }
}