<?php

namespace App\Http\Resources\Ayat;

use Illuminate\Http\Resources\Json\JsonResource;

class AyatDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ayatDetail = $this->resource;
        return [
            'id' => $ayatDetail->id,
            'ayat_number' => $ayatDetail->ayat_number,
            'surah_name' => $ayatDetail->surah_name,
            'surah_number' => $ayatDetail->surah_number,
            'book_name' => $ayatDetail->book_name,
            'book_number' => $ayatDetail->book_number,
            'volume_number' => $ayatDetail->volume_number,
            'title_arabic' => $ayatDetail->title_arabic,
            'title_urdu' => $ayatDetail->title_urdu,
            'title_english' => $ayatDetail->title_english,
            'description' => $ayatDetail->description,
            'created_at' => $ayatDetail->created_at,
            'updated_at' => $ayatDetail->updated_at,
        ];
    }
}
