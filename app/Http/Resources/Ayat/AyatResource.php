<?php

namespace App\Http\Resources\Ayat;

use Illuminate\Http\Resources\Json\JsonResource;

class AyatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ayat = $this->resource;
        return [
            'id' => $ayat->id,
            'title' => $ayat->title,
            'search_title' => $ayat->search_title,
            'ayat_detail' => AyatDetailResource::collection($ayat->ayat_detail),
            'created_at' => $ayat->created_at,
            'updated_at' => $ayat->updated_at,
        ];
    }
}
