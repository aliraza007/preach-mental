<?php

namespace App\Http\Resources\Location;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $location = $this->resource;
        return [
            'place_id' => $location->place_id,
            'address_1' => $location->address_1,
            'address_2' => $location->address_2,
            'address_3' => $location->address_3,
            'city' => $location->city,
            'county' => $location->county,
            'timezoneId' => $location->timezoneId,
            'region' => $location->region,
            'postcode' => $location->postcode,
            'latitude'=> $location->latitude,
            'longitude'=> $location->longitude, 
            'country'=> $location->country, 
            
        ];
    }
}
