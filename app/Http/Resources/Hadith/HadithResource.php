<?php

namespace App\Http\Resources\Hadith;

use Illuminate\Http\Resources\Json\JsonResource;

class HadithResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $hadith = $this->resource;
        return [
            'id' => $hadith->id,
            'title' => $hadith->title,
            'search_title' => $hadith->search_title,
            'hadith_detail' => HadithDetailResource::collection($hadith->hadith_detail),
            'created_at' => $hadith->created_at,
            'updated_at' => $hadith->updated_at,
        ];
    }
}
