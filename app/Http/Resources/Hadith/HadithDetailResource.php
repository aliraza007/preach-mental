<?php

namespace App\Http\Resources\Hadith;

use Illuminate\Http\Resources\Json\JsonResource;

class HadithDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $hadith = $this->resource;
        return [
            'id' => $hadith->id,
            'title' => $hadith->title,
            'hadith_number' => $hadith->hadith_number,
            'book_name' => $hadith->book_name,
            'book_number' => $hadith->book_number,
            'volume_number' => $hadith->volume_number,
            'reference_name' => $hadith->reference_name,
            'title_arabic' => $hadith->title_arabic,
            'title_urdu' => $hadith->title_urdu,
            'title_english' => $hadith->title_english,
            'description' => $hadith->description,
            'created_at' => $hadith->created_at,
            'updated_at' => $hadith->updated_at,
        ];
    }
}
