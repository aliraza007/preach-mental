<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Location\LocationResource;

class UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $userProfile = $this->resource;

        return [
            'id' => $userProfile->id,
            'distance_preference' => $userProfile->distance_preference,
            'description' => $userProfile->description,
            'location' => LocationResource::make($userProfile->location),

        ];
    }
}
