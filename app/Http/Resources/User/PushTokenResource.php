<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class PushTokenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $device = $this->resource;

        return [
            'udid' => $device->udid,
            'token' => $device->token,
            'device' => $device->device,
        ];
    }
}
