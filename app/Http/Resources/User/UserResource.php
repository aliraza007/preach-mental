<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use App\Http\Resources\User\PushTokenResource;
use App\Http\Resources\User\UserProfileResource;
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->resource;

        $output = [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'mobile_number' => $user->mobile_number,
            // 'avatar_src' => ($user->is_social) ? $user->avatar_src : $user->getAvatarAttribute(),
            'avatar_src' => ($user->is_social) ? $user->avatar_src : "",
            'status' => $user->status->getStatus(),
            'unique_code' => $user->unique_code,
            'sign_in_count'   => $user->sign_in_count,
            'last_active_at'   => empty($user->last_active_at) ? Carbon::now() : $user->last_active_at,
            'activated_at'    => $user->created_at,
            'notification_enabled' => $user->notification_enabled,
//            'biometric_enabled' => $user->biometric_enabled,
            'devices' =>  PushTokenResource::collection($user->devices),
            'access_token' => $user->token,
        ];

        if ($user->devices->isEmpty()) {
            Arr::forget($output, 'devices');
        }

        if (empty($user->unique_code)) {
            Arr::forget($output, 'unique_code');
        }

        if (empty($user->token)) {
            Arr::forget($output, 'access_token');
        }


        return $output;
    }
}
