<?php

namespace App\Http\Resources\Leader;

use Illuminate\Http\Resources\Json\JsonResource;

class LeaderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $leaderDetail = $this->resource;
        return [
            'id' => $leaderDetail->id,
            'heading' => $leaderDetail->heading,
            'paragraph' => $leaderDetail->paragraph,
            'created_at' => $leaderDetail->created_at,
            'updated_at' => $leaderDetail->updated_at,
        ];
    }
}
