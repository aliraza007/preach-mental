<?php

namespace App\Http\Resources\Leader;

use Illuminate\Http\Resources\Json\JsonResource;

class LeaderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $leader = $this->resource;
        $details = $leader->details;
        return [
            'id' => $leader->id,
            'name' => $leader->getName(),
            'details' => LeaderDetailResource::collection($details),
            'created_at' => $leader->created_at,
            'updated_at' => $leader->updated_at,
        ];
    }
}
