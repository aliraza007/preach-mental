<?php

namespace App\Http\Controllers\Api\v1\Leader;

use App\Http\Controllers\Controller;
use App\Http\Resources\Leader\LeaderDetailResource;
use App\Models\LeaderDetail;
use Illuminate\Http\Request;
use App\Core\Traits\ResponseTrait;


class LeaderDetailController extends Controller
{
    use ResponseTrait;
    public function getLeaderDetails()
    {
        try {

            $leaderDetail = LeaderDetail::with([
                'leader'
            ])->get();

            return $this->responseGet('Retrieved Leader Detail', LeaderDetailResource::collection($leaderDetail));

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }

    }
    //
}
