<?php

namespace App\Http\Controllers\Api\v1\Leader;

use App\Core\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Leader\LeaderDetailResource;
use App\Http\Resources\Leader\LeaderResource;
use App\Models\Leader;
use App\Models\LeaderDetail;
use Illuminate\Http\Request;

class LeaderController extends Controller
{
    use ResponseTrait;
    public function getLeader()
    {
        try {

            $leader = Leader::with([
                'details'
            ])->get();

            return $this->responseGet('Retrieved Leader Detail', LeaderResource::collection($leader));

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }
}
