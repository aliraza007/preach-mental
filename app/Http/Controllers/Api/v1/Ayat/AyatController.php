<?php

namespace App\Http\Controllers\Api\v1\Ayat;

use App\Core\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Ayat\AyatResource;
use App\Models\Ayat;

class AyatController extends Controller
{
    use ResponseTrait;
    public function getAyat()
    {
        try {
            $ayat = Ayat::with('ayat_detail')->get();
            return $this->responseGet('Retrieved ayats', AyatResource::collection($ayat));

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }
}
