<?php

namespace App\Http\Controllers\Api\v1\Hadith;

use App\Core\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Hadith\HadithResource;
use App\Models\Hadith;
use App\Models\Leader;
use Illuminate\Http\Request;

class HadithController extends Controller
{
    use ResponseTrait;
    public function getHadith()
    {
        try {
            $leader = Hadith::with('hadith_detail')->get();
            return $this->responseGet('Retrieved hadith', HadithResource::collection($leader));

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }
}
