<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Core\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use ResponseTrait;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {
            $request->user()->token()->revoke();
            return $this->responseGet('User Logout successfully');

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserDetail(Request $request)
    {
        try {
             $user = $request->user();
             if(empty($user)){
                 throw new \Exception('user not found!');
             }

            return $this->responseGet('User detail', UserResource::make($user));

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }


    //
}
