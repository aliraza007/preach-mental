<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Core\Events\User\UserCreatedEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\User\UserResource;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\User;
use App\Models\PushToken;
use App\Models\SocialUser;
use App\Core\Traits\ResponseTrait;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    use ResponseTrait;

    const USER_ROLE_ID = 2;
    protected $guard = "api";


    public function login(Request $request)
    {
        try {
            $attributes = [
                'email' => 'Email Address ',
                'password' => 'Password',
                'devices.udid' => 'Udid',
                'devices.token' => 'Device token',
                'devices.device' => 'Device name',
            ];

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|exists:users,email',
                'password' => 'required',
                'devices.udid' => 'string',
                'devices.token' => 'required|string',
                'devices.device' => 'required|string',
            ])->setAttributeNames($attributes);

            if ($validator->fails()) {
                return $this->validationError($validator->errors());
            }

            $email = filter_var($request->email, FILTER_SANITIZE_EMAIL);
            if (!Auth::attempt(['email' => $email, 'password' => $request->password])) {
                throw new \Exception('invalid user name and password');
            }

            $user = Auth::user();
            $role = $user->roles()->first();

            if ($role->id!=self::USER_ROLE_ID) {
                throw new \Exception('User Not Found!');
            }
            $user->sign_in_count++;
            $user->last_active_at = Carbon::now();
            $user->token = $user->createToken($email)->accessToken;
            if ($user->save()) {
                User::updateDevice($user, $request->get('devices'));
            }

            return $this->responseGet('User login successfully', UserResource::make($user));

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }


    /**
     * @param SocialUser $socialUser
     * @param $user
     * @return bool
     */
    public static function updateSocialUser(SocialUser $socialUser, $user = null)
    {
        if (!empty($user)) {

            if ($socialUser->user()->update($user))
                return true;
        }
        return false;
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(Request $request)
    {
        try {
            $attributes = [
                'email' => 'Email Address',
            ];

            $validator = Validator::make($request->only(['email']), [
                'email' => 'required|email|exists:users,email',
            ])->setAttributeNames($attributes);

            if ($validator->fails()) {
                return $this->validationError($validator->errors());
            }

            $user = User::where('email', $request->email)->first();
            if (empty($user)) {
                throw new \Exception('invalid user email!');
            }

            $user->forgot_code = str::random(6);
            $user->forgot_code_created_at = Carbon::now()->addMinutes(60);

            if (!$user->save()) {
                throw new \Exception('Failed to forgot password!');
            }

            $template = 'mailtemplates/forgot-password';
            $subject = 'Set your new preach password';
            $data = ['email'=>$user->email,'name'=>$user->name,'forgot_code'=>$user->forgot_code,'expire_date'=>$user->forgot_code_created_at];

            $this->sendEmail($user->email,$user->name,$data,$template,$subject);
            return $this->responseGet('email send please check your mail');
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }

    public function newPassword(Request $request)
    {
        try {
            $attributes = [
                'email'=>'Email',
                'password' => 'Password',
                'forgot_code'=>'Forgot Code'
            ];
            $validator = Validator::make($request->all() ,[
                'email' => 'required|email|exists:users,email',
                'password' => 'required|min:6',
                'forgot_code' => 'required|max:10',
            ])->setAttributeNames($attributes);

            if ($validator->fails()) {
                return $this->validationError($validator->errors());
            }

            $verifyUser = User::where(['forgot_code'=>$request->forgot_code,'email'=>$request->email])->first();
            if(!empty($verifyUser) && !empty($verifyUser->forgot_code)){
                if($verifyUser->forgot_code_created_at > now()){
                    $verifyUser->password=Hash::make($request->password);
                    $verifyUser->forgot_code = '';
                    if(!$verifyUser->save()){
                        throw new \Exception('Network error try again!!');
                    }
                    return $this->responseGet('Your password updated successfully');
                } else {
                    throw new \Exception('Yours code expired,please try again forgot password!');
                }
            } else {
                throw new \Exception('Forgot Code is invalid!');
            }
        }catch (\Exception $e){
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }

    public function socialLogin(Request $request)
    {
        try {
            $attributes = [
                'email' => 'Email Address ',
                'social_type' => 'Social Type',
                'name' => 'Name',
                'avatar_src' => 'Avatar Src',
                'social_token' => 'Social Token',
                'devices.udid' => 'Udid',
                'devices.token' => 'Device token',
                'devices.device' => 'Device name',
            ];

            $validator = Validator::make($request->all(), [
                'social_type' => 'required|string|' . Rule::in('facebook', 'google'),
                'email' => 'required_if:social_type,google',
                'name' => 'required|string',
                'avatar_src' => 'string',
                'social_token' => 'required|string',
                'devices.udid' => 'string',
                'devices.token' => 'required|string',
                'devices.device' => 'required|string',
            ])->setAttributeNames($attributes);

            if ($validator->fails()) {
                return $this->validationError($validator->errors());
            }

            $socialUser = SocialUser::where(['social_token' => $request->get('social_token'), 'social_type' => $request->get('social_type')])->first();
            if ($socialUser) {
                $name =  $request->get('name', null);
                $avatar_src =  $request->get('avatar_src', null);
                $email = $request->get('email', null);
                $user = [
                    'email' => $email,
                    'name' => $name,
                    'avatar_src' => $avatar_src,
                    'last_active_at' => Carbon::now(),
                ];
                if (empty($email)) {
                    Arr::forget($user, 'email');
                }
                if (empty($name)) {
                    Arr::forget($user, 'name');
                }
                if (empty($avatar_src)) {
                    Arr::forget($user, 'avatar_src');
                }
                static::updateSocialUser($socialUser, $user);
                if (!Auth::loginUsingId($socialUser->user->id)) {
                    throw new \Exception( "invalid user name and password");
                }
                $socialUser->user->sign_in_count++;
                $socialUser->user->last_active_at = Carbon::now();
                $socialUser->user->save();
                $socialUser->user->refresh();
                $socialUser->user = Auth::user();
                $socialUser->user->token  = $socialUser->user->createToken($socialUser->user->email)->accessToken;
                return $this->responseGet('User login successfully', UserResource::make($socialUser->user));
            }

            $user = new User();

            $user->email = $request->get('email', null);
            $user->password = $request->get('password', null);
            $user->name =  $request->get('name');
            $user->mobile_number =  $request->get('mobile_number', null);
            $user->avatar_src =  $request->get('avatar_src', null);
            $social = [
                'social_type' => $request->get('social_type'),
                'social_token' => $request->get('social_token'),
            ];
            $newUser = $this->create($user, $request->get('devices'), true, $social,$request);

            if ($newUser) {
                Event::dispatch(new UserCreatedEvent($newUser));
                return $newUser;
            }
            return $this->responseGet('User login successfully', UserResource::make($newUser));

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signUp(Request $request)
    {
        try {
            $attributes = [
                'email' => 'Email Address ',
                'name' => 'Name',
                'password' => 'Password',
                'devices.udid' => 'string',
                'devices.device' => 'string',
                'devices.token' => 'string',
            ];

            $validator = Validator::make($request->all(), [
                'email' => 'required|string|unique:users,email|',
                'password' => 'required|string',
                'name' => 'required|string',
                'devices.udid' => 'string',
                'devices.token' => 'string',
                'devices.device' => 'string',
            ])->setAttributeNames($attributes);

            if ($validator->fails()) {
                return $this->validationError($validator->errors());
            }

            $user = $this->createUser($request);

            return $this->responseGet('User register successfully', UserResource::make($user));

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), null, $e->getCode());
        }
    }

    /**
     * @param null $data
     * @return \Illuminate\Contracts\Auth\Authenticatable|\Illuminate\Http\JsonResponse|null
     * @throws \Exception
     */
    protected function createUser($data = null)
    {
        $user = new User();
        $user->email = $data->get('email');
        $user->password = $data->get('password');
        $user->name =  $data->get('name');
        $newUser = $this->create($user, $data->get('devices'));
        if (!empty($newUser)) {
            return $newUser;
        }
        return null;
    }

    /**
     * @param User $user
     * @param array|null $device
     * @param bool $is_social
     * @param array|null $social
     * @return \Illuminate\Contracts\Auth\Authenticatable|\Illuminate\Http\JsonResponse|null
     * @throws \Exception
     */
    protected function create(User $user, array $device = null, $is_social=false, array $social = null)
    {
        DB::beginTransaction();
        if (!empty($user->password)) {
            $password = $user->password;
            $user->password = Hash::make($user->password);
        }
        $user->sign_in_count++;
        $user->is_loggedin = true;
        $user->is_social = $is_social;
        $user->unique_code = $user->generateUniqueCode();
        $user->last_active_at = Carbon::now();
        $user->pin_code = rand(00000,99999);
        if (!$user->save()) {
            DB::rollback();
            throw new \Exception('Failed to add user!');
        }

        if (!empty($device)) {
            $deviceToken = Arr::get($device, 'token');
            $deviceType = Arr::get($device, 'device');
            $deviceUdid = Arr::get($device, 'udid');
            $pushToken = new PushToken();
            $pushToken->udid = $deviceUdid;
            $pushToken->token = $deviceToken;
            $pushToken->device = $deviceType;
            $pushToken->user()->associate($user);
            if (!$pushToken->save()) {
                DB::rollback();
                throw new \Exception('Failed to add  Push Token!');
            }
        }
        $user->roles()->attach(static::USER_ROLE_ID);
        if ($is_social && !empty($social)) {
            $socialToken = Arr::get($social, 'social_token');
            $socialType = Arr::get($social, 'social_type');
            $socialUser = new SocialUser();
            $socialUser->social_token = $socialToken;
            $socialUser->social_type = $socialType;
            $socialUser->user()->associate($user);
            if (!$socialUser->save()) {
                DB::rollback();
                throw new \Exception('Failed to add social user!');
            }
            DB::commit();
            $user->refresh();
            if (!Auth::loginUsingId($socialUser->user->id)) {
                throw new \Exception( "invalid user name and password");
            }
            $user = Auth::user();
            $user->token = $user->createToken($user->email)->accessToken;
            return $user;
        }
        DB::commit();
        $user->refresh();
        if (!Auth::attempt(['email' => $user->email,'password'=>$password])) {
            throw new \Exception( "invalid user name and password");
        }
        $user = Auth::user();

        $user->token = $user->createToken($user->email)->accessToken;
        return $user;
    }
}
